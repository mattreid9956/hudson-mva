#!/bin/bash

set -x

cd ${0%%$(basename $0)}
mkdir -p build
cd build
# Build using maximum number of physical cores
n=`cat /proc/cpuinfo | grep "cpu cores" | uniq | awk '{print $NF}'`

cmake -DCMAKE_BUILD_TYPE=DEBUG .. && make -j $n  # && make test

