// Include files 

// local
#include "ITMVAFactory.hpp"

// So the TMVA networks pick up the std:: functions
using namespace std;

namespace NN
{

  /** @namespace MC12TuneV2 ITMVAFactory.cpp
   *
   *  Namespace for C++ implementations of MC12TuneV2 TMVA networks.
   *
   *  @author Matthew M Reid  
   *  @date   2013-07-29
   */

  // Standard constructor
  ITMVAFactory::ITMVAFactory() {
    // Long
    add<MC12TuneV2::ReadElectron_Long_TMVA>       ( "MC12TuneV2", "electron", "Long" );
    add<MC12TuneV2::ReadMuon_Long_TMVA>           ( "MC12TuneV2", "muon",     "Long" );
    add<MC12TuneV2::ReadPion_Long_TMVA>           ( "MC12TuneV2", "pion",     "Long" );
    add<MC12TuneV2::ReadKaon_Long_TMVA>           ( "MC12TuneV2", "kaon",     "Long" );
    add<MC12TuneV2::ReadProton_Long_TMVA>         ( "MC12TuneV2", "proton",   "Long" );
    add<MC12TuneV2::ReadGhost_Long_TMVA>          ( "MC12TuneV2", "ghost",    "Long" );
  }  

  ITMVAFactory::~ITMVAFactory( ) {

    for ( Map::iterator i = m_map.begin(); i != m_map.end(); ++i ) { 
      delete i->second; 
    }
  
  }

  // Method to get a static instance of the factory
  const ITMVAFactory& tmvaFactory()
  {
    static ITMVAFactory factory;
    return factory;
  }

}
