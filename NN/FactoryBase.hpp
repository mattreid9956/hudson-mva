#ifndef FactoryBase_HPP
#define FactoryBase_HPP 1

// STL headers
#include <vector>

// local include
#include "IClassifierReader.hpp"

namespace NN {

    class FactoryBase {

      public:
        virtual IClassifierReader* create( std::vector<std::string> inputs ) = 0;
        virtual ~FactoryBase() { }
    };

} // end namespace NN

#endif // FactoryBase_HPP
