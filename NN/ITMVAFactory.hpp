#ifndef ITMVAFactory_HPP
#define ITMVAFactory_HPP 1

// STL include
#include <map>
#include <vector>
#include <cmath>
#include <exception>

// local include
#include "IClassifierReader.hpp"
#include "FactoryBase.hpp"

namespace NN {

  /** @class ITMVAFactory ITMVAFactory.hpp
  *
  *  Factory to create instances of standalone C++ TVMA objects for
  *  a given time series and network tuning.
  *
  *  @author Matthew M Reid
  *  @date   2013-07-12
  */

  class ITMVAFactory  {

    private:

      /** @class FactoryBase TMVAImpFactory.h
       *
       *  Base class for factories
       *
       *  @author Matthew M Reid
       *  @date   2013-07-12
       */

      class FactoryBase {
        public:
          /// Create an instance of the TMVA classifier for this factory
          virtual IClassifierReader* create( std::vector<std::string> inputs ) = 0;
          /// Destructor
          virtual ~FactoryBase() { }
      };
      
      /** @class TMVAFactory TMVAFactory.hpp
       *
       *  Templated class for specific TMVA factories
       *
       *  @author Matthew M Reid
       *  @date   2013-07-12
       */
    
      template <class TMVATYPE>
      class TMVAFactory : public FactoryBase {
        
        public:
        inline IClassifierReader* create( std::vector<std::string> inputs ) {
          return new TMVATYPE(inputs); 
        }
        
          virtual ~TMVAFactory() { }
      };
    
    public:

      TMVAFactory( );

      ~TMVAFactory( );

    private:

      inline std::string id( const std::string & config,
                             const std::string & particle,
                             const std::string & track ) const
      {
        return config+"-"+particle+"-"+track ;
      }

      template <class TMVATYPE>
      inline void add( const std::string & config,
                       const std::string & particle,
                       const std::string & track )
      {
        const std::string _id = id(config,particle,track);
        Map::const_iterator i = m_map.find( _id );
        if ( i != m_map.end() )
        {
          throw std::exception( _id+" already registered", 
                                "ANNGlobal::TMVAFactory",
                                StatusCode::FAILURE );
        }
        m_map[ _id ] = new TMVAFactory<TMVATYPE>();
      }

    public:

      inline 
      IClassifierReader* create( const std::string & config,
                                 const std::string & particle,
                                 const std::string & track,
                                 const std::vector<std::string>& inputs ) const
      {
        Map::const_iterator i = m_map.find( id(config,particle,track) );
        return ( i != m_map.end() ? i->second->create(inputs) : 0 );
      }
    
    private:

      typedef std::map< std::string, FactoryBase* > Map;

      Map m_map;

  };

  // declare TMVA Factory
  const TMVAFactory& tmvaFactory();

}

#endif // ITMVAFactory_HPP

