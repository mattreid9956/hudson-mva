// My test of the class functionality

#include <iostream>
#include <vector>
#include <string>

// include local
#include "DefinedMVAs.hpp"
#include "TMVAClassification.hpp"
#include "TMVAReader.hpp"

// include root
#include "TFile.h"
#include "TTree.h"

bool trainer = true;
bool evaluate = true;


int main() {
  
  //std::string outname("TMVA.root"); // used by default anyways
  std::vector<std::string> mvaMethods, inputvars;
  mvaMethods.push_back("Cuts");
  mvaMethods.push_back("LD");
  mvaMethods.push_back("SVM");
  mvaMethods.push_back("MLP");
  mvaMethods.push_back("BDT");
  mvaMethods.push_back("BDTB");
  mvaMethods.push_back("BDTD");
  mvaMethods.push_back("BDTG");
  //mvaMethods.push_back("PDERS");
  mvaMethods.push_back("BoostedFisher");
  mvaMethods.push_back("TMlpANN");
  mvaMethods.push_back("CFMlpANN");

  //inputvars.push_back("MAXDOCACHI2");
  inputvars.push_back("B_ETA");
  inputvars.push_back("B_MINIPCHI2");
  inputvars.push_back("B_DIRA_OWNPV");
  inputvars.push_back("B_ENDVERTEX_PROB");
  //inputvars.push_back("B_CTAUSIG_PVKSJpsi");
  inputvars.push_back("KS_DLS_OWNPV");
  inputvars.push_back("KS_ENDVERTEX_PROB");
  inputvars.push_back("Jpsi_DLS_OWNPV");
  inputvars.push_back("Jpsi_MINIPCHI2");
  inputvars.push_back("Jpsi_ENDVERTEX_PROB");
  inputvars.push_back("min(log10(h1_MINIPCHI2), log10(h2_MINIPCHI2))");
  //inputvars.push_back("h1_MINIPCHI2");
  //inputvars.push_back("h2_MINIPCHI2");

  std::string DDLL("LL");

  std::string treepath = "tupleB2JpsiKSpipi" + DDLL;
  //std::string inputrootfile = "B2JpsiKShh-Collision11-Merged-Stripping17b-doca_cropDataOneCand.root";
  std::string inputrootfile = "B2JpsiKShh-Stripping17b_"+DDLL+"_withSWeight.root";

  if( trainer ) {
    NN::TMVAClassification* classifier = new NN::TMVAClassification( mvaMethods, inputvars);


    TFile *file = TFile::Open( inputrootfile.c_str() );
    TTree *treeS = dynamic_cast< TTree* > ( file->Get( treepath.c_str() ) );
    TTree *treeB = dynamic_cast< TTree* > ( file->Get( treepath.c_str() ) );

    //treeS = treeS->CopyTree( "nsig_S_sw" );
    //treeB = treeB->CopyTree( "nbkg_B_sw" );
    // set the signal and background trees
    //classifier->setSignalAndBackgroundTrees( treeS, treeB, 1., 1.);
  
    // set the weighting expressions if you have them in the trees.
    //classifier->setBackgroundWeightExpression( "nsig_S_sw" );
    //classifier->setSignalWeightExpression( "nbkg_B_sw" );
    //classifier->prepareTestAndTraining("SOME WIERD STRING SETTINGS");
    //classifier->bookMethods();
  
    classifier->basicSetup( treeS, treeB, "nsig_S_sw", "nbkg_B_sw" );
    classifier->setOptions( "TMlpANN", NN::DefinedMVAs.find("TMlpANN")->second + ":VarTransform=D,G_Signal,N" );
    classifier->train( );
    delete classifier; classifier = 0;
  }
 
  if( evaluate ) {
    NN::TMVAReader* reader = new NN::TMVAReader( mvaMethods, inputvars, "outputFile.root" );
    reader->setFileAndTree( inputrootfile, treepath );
    reader->fillTuple();

    delete reader; reader = 0;
  }
 
  return 0;
}
