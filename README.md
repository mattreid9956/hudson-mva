File: $Id: README.md,v 1.25 2013/09/04 13:29:20 mreid Exp $
Project: Trading simulator based on EOD historical data. Allows backtesting, statistical testing, and Multivariate Analyses
Package: Hudson

Author: Alberto Gianetti albertogiannetti@gmail.com
Copyright � 2008-2013
Description: This file contains package information and usage instructions
Contributors: Matthew M Reid, matthew.reid@gmail.com


********************************************************************************

# General information

********************************************************************************

Hudson is a free, open-source trading simulator based on EOD price historical data. It is designed as a C++ library providing simulation and statistics tools for integration with other trading strategy applications. A set of strategy examples is included in the source code distribution to illustrate some of the features provided. This software is open source. You are required to redistribute any change you make to the original source code following the GPLv3 license agreement. Please read the COPYING file for more information on GPL licensing.

Hudson calculates various statistics, including compound annualized growth rate, % winners/losers, realized drawdown, position excursion analysis, month-to-month returns, Sharpe ratio and geometric standard deviation of monthly returns. The report statistics are easily extensible by inheriting from the Report class and adding your own calculation based on recorded transactions and historical data.

The trader API supports backtesting of any custom long/short trading strategy, such as portfolio backtesting of multiple symbols, pairs-trading and spread-trading strategies. For a spread trading example, check the January trader (JanTrader?). This class implements a trading strategy analyzing the seasonal microcap effect that occurs around the end of the year.

********************************************************************************

# What does this package depend on?

********************************************************************************

Hudson requires the GNU Scientific Library (GSL), TA-Lib, and the Boost libraries.

1) cmake - for easy compilation see http://www.cmake.org/ for more information
2) GSL - GNU Scientific Library. The GSL is a free, open source library developed by the FSF offering over 1000 numerical functions including statistics, least-square fitting, random numbers and Monte Carlo integration: http://www.gnu.org/software/gsl/.
3) Boost - provides a set of free, open source portable C++ libraries implementing a wide range of functionalities, from a complete set of date and time functions to multi-index collections and objects serialization: http://www.boost.org/libs/libraries.htm.
4) TA-LIB - a free, open-source financial series technical analysis library maintained by Mario Fortier. It offers support to more than 150 technical analysis indicators in C++, Java, Excel, .NET and other languages: http://ta-lib.org.

Optional builds
5) CERN's ROOT Analysis Framework. In fact this part of the package is basically a 
front-end for performing some of the TMVA (multivariate analysis) methods.
 

********************************************************************************

# How do I build the package?

********************************************************************************

You can access this repo with SSH or HTTPS;
Only tested on Linux. 

Either you have got the source or got it from the repo, you must then

cd Hudson
./build.sh

Thats it!! If you are using another shell environment such as .csh, 
view the build.sh script and adapt it as you require is essentially
makes a directory called build in the top Hudson directory
and puts all the libraries and examples into here via cmake. At present there is 
no default install setup.

please check that the examples work for you.

********************************************************************************

# Where are the multivariate analysis examples?

********************************************************************************
There are two examples for MVAs in this analysis package. The first is trained
specifically using the quantitative indicators such as MACD, APO etc. This example 
comes under

    examples/MVA_SNP500 (requies ROOT)- ./run.sh to execute. Th default produces the following 
    and works using a BDT with a cut vlue placed at 0.01. Feel free to try the other MVA, remember however
    non of these are optimised for you, that would take the fun away and be the same as handing you a golden nugget,
    
	ROI: 13191.40%
	CAGR: 202.58%
	GSDm: 109.32%
	Sharpe: 1.63
	Trades: 663
	Avg trade: 0.78%
	Std dev: 2.90%
	Skew: 2.38
	2SD Range: -5.02% | 6.58%
	3SD Range: -7.91% | 9.48%
	Pos trades: 441 (66.52%)
	Neg trades: 221 (33.33%)
	Avg pos: 2.24%
	Avg neg: -2.12%
	Best: 17.47% [2009-Mar-10/2009-Mar-19]
	Worst: -13.84% [2011-Jul-29/2011-Aug-09]
	Max cons pos: 45 [2010-Feb-05/2010-Apr-26]
	Max cons neg: 12 [2010-Apr-16/2010-May-17]
	Max drawdown: -54.41% [2011-May-03/2011-Oct-07]
	
	Adverse Excursion
	Avg AE: -1.78%
	Longest AE:  Position 537 [2012-Jul-03/2012-Jul-12]
	Worst AE: -13.37% Position 392 [2011-Jul-29/2011-Aug-08]

	Favorable Excursion
	Average FE: 2.41%
	Longest FE:  Position 90 [2009-Oct-02/2009-Oct-12]
	Best FE: 15.02% Position 9 [2009-Mar-09/2009-Mar-17]

 
    the second comes from strategies using Google trends, which is considered and sentiment indicator to model
    what people are interested in at the time and see their correlation with the stock market. See ref GoogleTrend paper for info
    MVA_GOOGLETREND (requires ROOT) - run the shell script for basic setup run.sh. This trains a network based on certain Google
    trend words.


********************************************************************************

# Where are the examples?

********************************************************************************

There are commented example programs under the examples directory in the package.
These are compiled programs, not ROOT macros. 
They exist in the build/examples directory.
To compile them go into the directory of the specific example and run it:

./example

Examples currently test and in a working state are:

    AATrader - enters trades in 5 different asset classes to optimize risk-adjusted returns. SP500, US Bonds,
    Commodities, EAFE and REIT are the databases currently supported. Uses SMA to define stategy.
 
    AATraderMACD - enters trades in 5 different asset classes to optimize risk-adjusted returns. SP500, US Bonds,
    Commodities, EAFE and REIT are the databases currently supported. Uses MACD to define strategy.

    VIXTrader - Uses the S&P500 volitility index (VIX) to calculate bollinger bands to trade the S&P500 asset.

    BOWTrader - Trades at the start of each week if markets is bearish, and shorts the position.

    EOMTrader - Trades at the end of each month, no strategy.

    JANTrader - Trades at the start of each year (Jan), hedges the trade with buy and short signal.

WARNINGS: - Some of the example programs take a long time to execute - you may wish to edit them e.g. by shortening a loop.


********************************************************************************

# Credits

********************************************************************************

Alberto should take all the credit here. Thanks for a great app!!


