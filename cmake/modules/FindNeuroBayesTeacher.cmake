# - Locate NeuroBayesTeacher library
# Defines:
#
#  NEUROBAYES_FOUND
#  NEUROBAYES_INCLUDE_DIR
#  NEUROBAYES_INCLUDE_DIRS (not cached)
#  NEUROBAYES_LIBRARIES
#  NEUROBAYES_LIBRARY_DIRS (not cached)

MESSAGE(STATUS "Looking for NeuroBayes...")
SET(NEUROBAYES_SEARCHPATH $ENV{NEUROBAYES}/include $ENV{NEUROBAYES}/lib )
# - Provide cmake interface to Neurobayes libraries.
#include(FindNeuroBayesExpert)
#include(FindNeuroBayesTeacher)
SET(NEUROBAYES_LIB_DIR $ENV{NEUROBAYES}/lib )
SET(NEUROBAYES_SEARCHPATH $ENV{NEUROBAYES}/include $ENV{NEUROBAYES}/lib )

# Include dir
find_path(NEUROBAYES_INCLUDE_DIR
  NAMES NeuroBayesTeacher.hh NeuroBayesExpert.hh
  PATHS ${NEUROBAYES_SEARCHPATH}
)

# Define a list of the libraries we will need
set(NEUROBAYES_NAMES ${NEUROBAYES_NAMES} NeuroBayesTeacherCPP NeuroBayesExpertCPP)

# Finally the library itself
find_library(NEUROBAYES_LIBRARIES
  NAMES ${NEUROBAYES_NAMES}
  PATHS ${NEUROBAYES_SEARCHPATH}
  )


# handle the QUIETLY and REQUIRED arguments and set NEUROBAYES_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(NEUROBAYES DEFAULT_MSG NEUROBAYES_INCLUDE_DIR NEUROBAYES_LIBRARIES)

mark_as_advanced(NEUROBAYES_FOUND NEUROBAYES_INCLUDE_DIR NEUROBAYES_LIBRARIES)

MESSAGE(STATUS "Found NeuroBayes Libraries... ${NEUROBAYES_LIBRARIES}")
MESSAGE(STATUS "Found NeuroBayes include... ${NEUROBAYES_INCLUDE_DIR}")
	
