
add_library(AATrader SHARED AATrader.cpp  AATrader.hpp)
add_executable(aa aa.cpp)

target_link_libraries(aa AATrader Hudson)
 
 
#FILE(GLOB_RECURSE COMPONENT_TXT_FILE_LIST "*.txt")
#FILE(COPY ${COMPONENT_TXT_FILE_LIST} DESTINATION .)

install(TARGETS aa
  # IMPORTANT: Add the aa executable to the "export-set"
  EXPORT HudsonTargets
  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin)
