#include "GoogleTrendReader.hpp"
#include "string_tools.hpp"

#include "TSystemDirectory.h"

GoogleTrendReader::GoogleTrendReader( const Series::EODSeries& db, const std::string& outputFileName, const std::string& directory ) 
: m_db( db ), m_treeWriter( outputFileName, "google" ), m_directory( directory+"/" ), m_dayshift(7), m_filesFound(false)
{
  m_inputFileNames.clear();
  m_treeWriter.add( "Day" );
  m_treeWriter.add( "Month" );
  m_treeWriter.add( "Year" );
  //m_treeWriter.add( "close" );
  m_treeWriter.add( "nbkg" );
  m_treeWriter.add( "nsig" );
}


//********************************************************************************
GoogleTrendReader::~GoogleTrendReader( ) 
{ 
  if( !m_indicatorApp.empty() ) {
    std::map< std::string, IndicatorApp* >::iterator iter = m_indicatorApp.begin();
    const std::map< std::string, IndicatorApp* >::iterator end = m_indicatorApp.end();
    for (; iter != end; ++iter ) {
      if( iter->second ) {
        delete iter->second; iter->second=0;
      }
    }	
      
  }
//delete m_treeWriter; m_treeWriter=0;
}


//********************************************************************************
bool GoogleTrendReader::getListOfFiles( const char *dirname, const char *ext, const int& period) {

  bool status = false;
  TSystemDirectory dir( dirname, dirname );
  TList *files = dir.GetListOfFiles();
  if( files->IsEmpty() )
    return status;
  files->Sort(kTRUE);
  boost::gregorian::date begin = m_db.begin()->first;
  boost::gregorian::date end = m_db.last().key;

  std::string name("");
  string_tools strtools;
  if ( files ) {
    TSystemFile *file = 0;;
    TString fname;
    TIter next(files);
    while ( ( file = ( TSystemFile* ) next() ) ) {
      fname = file->GetName();
      name = fname.Data();
      if ( !file->IsDirectory() && fname.EndsWith( ext ) ) {
        strtools.replaceAfter( name, "_" );
        Series::EODDB::instance().load( name, TString(dirname + fname).Data(), Series::EODDB::GOOGLETREND, begin, end );
        m_inputs.push_back( name );//m_inputFileNames.insert( std::make_pair( name, dbseries ) );
        std::cout << "ADDING TO IndicatorAPP for " << name << std::endl;
        //m_indicatorApp.insert( std::make_pair( name, Series::EODDB::instance().get( name ) ) );
        m_inputFileNames[name] = &Series::EODDB::instance().get( name );
	m_indicatorApp[name] = new IndicatorApp( Series::EODDB::instance().get( name )  );
        m_indicatorApp[name]->add( "GTND", period );
        m_indicatorApp[name]->initialise();
        m_treeWriter.add( "GTND_" + name );
	std::cout << "INFO: GoogleTrendReader - " << m_inputs.size() << " files have been read in."<< std::endl;
      } 
    }
  }
  status = true;
  return status;
}

//********************************************************************************
void GoogleTrendReader::setPeriodAndSignalShift( const int& period, const int& dayshift ) {
 
  std::cout << "INFO: GoogleTrendReader - adding Google trend files from directory " << m_directory << std::endl; 
  std::cout << "INFO: GoogleTrendReader - period is "<< period << " and days shifted to calculate weighting is " << dayshift << std::endl; 
  bool status =  getListOfFiles( m_directory.c_str(), ".csv", period ) ;
  if(!status) return;
  m_filesFound = true;
  m_iter = m_db.begin();
  m_enditer = m_db.end();
  m_dayshift = dayshift;
  // Advance the iterator to start of indicator data
  std::advance( m_iter, period + 1 );
  // stops hitting the last entry when calculating the signal like events, since we look to
  // the future.
  
  std::advance( m_enditer, -1. * m_dayshift );
}

//********************************************************************************
void GoogleTrendReader::writeData( ) {

  if (!m_filesFound ) {
    std::cerr << "ERROR: GoogleTrendReader - no files found with extension .csv in directory: " << m_directory << std::endl;
    exit(EXIT_FAILURE);
  }
  double invert(0.);
  //m_enditer = m_db.first_in_week( m_enditer->first.year(),  m_enditer->first.month(),  m_enditer->first.day() );
  //--m_enditer;
  std::cout << "Beginning writing to datafile.\n";
  
  //m_iter = m_db.after( m_indicatorApp.begin()->second->getStartDate() );
  std::vector< std::string >::const_iterator it = m_inputs.begin();
  const std::vector< std::string >::const_iterator endit = m_inputs.end();
  bool ok(true);
  boost::gregorian::date d1;
  Float_t change(0.0);
  while ( ok )
  {
    it = m_inputs.begin();
    for ( ; it != endit; ++it ) {
      std::string name = *it;
      //if( m_db.first_in_week( m_indicatorApp[name]->getCurrentIterDate() + boost::gregorian::days( m_dayshift ) ) == m_db.end() ) {
      if( m_indicatorApp[name]->getCurrentIterDate() + boost::gregorian::days( m_dayshift ) >=  m_indicatorApp[name]->getEndDate() ) {
	std::cout << "first in week not in range\n";
        ok = false;
	break;
      }
      ok = m_indicatorApp[name]->next();
      if( !ok ) {
	//std::cerr << "BREAKING!" << std::endl;
        break;
      }
      Float_t testValue = m_indicatorApp[name]->evaluate( "GTND", "" );
      Float_t value = ( testValue != Float_t(-std::numeric_limits<double>::max()) ) ? testValue : -99999.99 ;
      std::cout << name << " has GTND value " << value << std::endl;
	
      m_treeWriter.column( "GTND_"+name, value );
      d1 =  m_indicatorApp[name]->getCurrentIterDate();
    }
    
    if(ok) {
      change = calculatePercentage( d1 );
      //std::cout << change << std::endl; 
      if( change < 1) {
        invert = 1. + ( 1. - change );
        m_treeWriter.column( "nsig", static_cast<Float_t>( 0 ) );
        m_treeWriter.column( "nbkg", static_cast<Float_t>( invert ) );
      } else if ( change > 1) {
        m_treeWriter.column( "nsig", static_cast<Float_t>( change ) );
        m_treeWriter.column( "nbkg", static_cast<Float_t>( 0 ) );
      } 
      m_treeWriter.column( "Day", static_cast<Float_t>( d1.day() ) );
      m_treeWriter.column( "Month", static_cast<Float_t>( d1.month() ) );
      m_treeWriter.column( "Year", static_cast<Float_t>( d1.year() ) );
      m_treeWriter.write();
    }
  } 
 
/*  for ( ; m_iter != m_enditer; ++m_iter ) { 
    // now determine if signal like or not?
    Series::EODSeries::const_iterator shiftediter = m_iter;
    std::advance( shiftediter, m_dayshift );
    change = std::fabs( m_db.after( shiftediter->first )->second.close )/ (double) m_iter->second.close;
    //std::cout << change << std::endl; 
    if( change < 1) {
      invert = 1. + ( 1. - change );
      m_treeWriter.column( "nsig", static_cast<Float_t>( 0 ) );
      m_treeWriter.column( "nbkg", static_cast<Float_t>( invert ) );
    } else if ( change > 1) {
      m_treeWriter.column( "nsig", static_cast<Float_t>( change ) );
      m_treeWriter.column( "nbkg", static_cast<Float_t>( 0 ) );
    } else { 
       continue; 
    }

    // now loop over leaves in tuple
    it = m_inputs.begin();
    
    for ( ; it != endit; ++it ) {
      std::string name = *it;
      m_indicatorApp[name]->next();
      Float_t testValue = m_indicatorApp[name]->evaluateAtOrBefore( "GTND", m_iter->first, "" );
      Float_t value = ( testValue != Float_t(-std::numeric_limits<double>::max()) ) ? testValue : -99999.99 ;
      std::cout << name << " has GTND value " << value << std::endl;
      m_treeWriter.column( "GTND_"+name, value );
    }

  }*/

}


Float_t GoogleTrendReader::calculatePercentage( const boost::gregorian::date& date ) {

  Series::EODSeries::const_iterator present_iter = m_db.first_in_week(  date.year(),  date.month(),  date.day() );
  Series::EODSeries::const_iterator future_iter = present_iter;
  std::advance( future_iter, m_dayshift );
  
  return std::fabs( Float_t( m_db.after( future_iter->first )->second.close ) )/ (Float_t) present_iter->second.close;
    //std::cout << change << std::endl; 
}
