#ifndef GOOGLETRENDREADER_HPP
#define GOOGLETRENDREADER_HPP 1

#include <iosfwd>
#include <string>
#include <utility>
#include <vector>
#include <map>

#include <Database.hpp>

#include "TreeWriter.hpp"
#include "IndicatorApp.hpp"

//********************************************************************************
// 
// GoogleTrendReader class: Will pick up all the Google trend data in the format Data/Interest:-
// The files should be placed into the GoogleTrendsData folder
// They need not have a header
// 
//********************************************************************************
//const char delimiters[] = " \t\n;"

class GoogleTrendReader 
{
  public:
    explicit GoogleTrendReader( const Series::EODSeries& db, const std::string& outputFileName, const std::string& directory);

    virtual ~GoogleTrendReader();
    // Copy assignment operator
    //GoogleTrendReader& operator=(const GoogleTrendReader& other);
    void setPeriodAndSignalShift( const int& period = 3, const int& dayshift = 7 );
    // setters
    void writeData();
        
        // getters

  private:
    bool getListOfFiles( const char *dirname="/home/matt/hudson/db/GoogleTrends/", const char *ext=".csv", const int& period = 3 );
    Float_t calculatePercentage( const boost::gregorian::date& date );

    // private member functions
    const Series::EODSeries& m_db;
    NN::TreeWriter m_treeWriter;
    const std::string m_directory;
    std::map< std::string, const Series::EODSeries* > m_inputFileNames;
    std::vector< std::string > m_inputs;
    std::map< std::string, IndicatorApp* > m_indicatorApp;
    Series::EODSeries::const_iterator m_iter;
    int m_dayshift;
    bool m_filesFound;
    Series::EODSeries::const_iterator m_enditer;
};

#endif
