// My test of the class functionality

#include <iostream>
#include <vector>
#include <string>
#include <cmath>

// Boost
#include <boost/program_options.hpp>

// include local
#include "GoogleTrendReader.hpp"

// Hudson

#include <IndicatorApp.hpp>

// include root
#include "TFile.h"
#include "TTree.h"

bool trainer = true;
bool evaluate = true;

using namespace std;
using namespace boost::gregorian;
using namespace Series;
namespace po = boost::program_options;


int main(int argc, const char* argv[]) {

  std::string begin_date, end_date;
  std::string spx_dbfile, trendsDirectory;
  std::string outputFileName = "outputFile_Google_SPX.root";
  std::string treepath = "google";
  int dayshift = 7;
  int period = 3;
  /*
   * Extract simulation options
   */
  po::options_description desc("Allowed options");
  desc.add_options()
  	("help", "produce help message")
  	("spx_file",   po::value<std::string>(&spx_dbfile),     	"SPX series database")
  	("begin_date", po::value<std::string>(&begin_date),     	"start of trading period (YYYY-MM-DD)")
  	("end_date",   po::value<std::string>(&end_date),       	"end of trading period (YYYY-MM-DD)") 
  	("trend_dir",  po::value<std::string>(&trendsDirectory),     	"location of the Google trend files to be read in")
  	("day_shift",   po::value<int>(&dayshift),       		"time period (day shift) for calculating signal and background weights (7)") 
  	("iapp_period",   po::value<int>(&period),       		"the indicator app period for calculating the SMA difference (3)") 
  	("output_file",   po::value<std::string>(&outputFileName),   "output file (outputFile_Google_SPX.root)") 
  	("tree_path",   po::value<std::string>(&treepath),   		"tuple tree path for output root file (data)") ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if( vm.count("help") ) {
  	cout << desc << endl;
 	exit(0);
  }

  if( vm["spx_file"].empty() ||
	vm["begin_date"].empty() || vm["end_date"].empty() || vm["trend_dir"].empty() ) {
 	cout << desc << endl;
	exit(1);
	}


  try {
    /*
     * Load series data
     */
    Series::Database::SeriesFile sf;
    Series::Database::SERIES_MAP mSeries;
	date load_begin(from_simple_string(begin_date));
	if( load_begin.is_not_a_date() ) {
		cerr << "Invalid begin date " << begin_date << endl;
		exit(EXIT_FAILURE);
	}

	date load_end(from_simple_string(end_date));
	if( load_end.is_not_a_date() ) {
		cerr << "Invalid end date " << end_date << endl;
		exit(EXIT_FAILURE);
	}
	
    // Load the series	
    const string spx_symbol = "index:SPX";
    std::cout << "Loading " << spx_dbfile << " from " << load_begin << " to " << load_end << "..." << std::endl;
    Series::EODDB::instance().load(spx_symbol, spx_dbfile, Series::EODDB::YAHOO, load_begin, load_end);
    const Series::EODSeries& spx_db = Series::EODDB::instance().get( spx_symbol );
   
    // Initialize and run strategy
    GoogleTrendReader google( spx_db, outputFileName, trendsDirectory );
    google.setPeriodAndSignalShift( period, dayshift );
    std::cout << "WRITING FILE.\n";
    google.writeData();

  } catch( std::exception& ex ) {

    std::cerr << "Unhandled exception: " << ex.what() << std::endl;
    exit(EXIT_FAILURE);
  }

 
 
  return 0;
}
