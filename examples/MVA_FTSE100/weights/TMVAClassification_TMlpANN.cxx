#include "weights/TMVAClassification_TMlpANN.h"
#include <cmath>

double TMVAClassification_TMlpANN::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6) {
   input0 = (in0 - -5.60505e-10)/1.07791;
   input1 = (in1 - -4.81394e-10)/1.04914;
   input2 = (in2 - -1.10968e-10)/1.0369;
   input3 = (in3 - -3.43072e-11)/1.00031;
   input4 = (in4 - 5.83026e-11)/0.971169;
   input5 = (in5 - -1.87829e-10)/0.963764;
   input6 = (in6 - 1.06075e-10)/0.902977;
   switch(index) {
     case 0:
         return neuron0x37e41b0();
     default:
         return 0.;
   }
}

double TMVAClassification_TMlpANN::Value(int index, double* input) {
   input0 = (input[0] - -5.60505e-10)/1.07791;
   input1 = (input[1] - -4.81394e-10)/1.04914;
   input2 = (input[2] - -1.10968e-10)/1.0369;
   input3 = (input[3] - -3.43072e-11)/1.00031;
   input4 = (input[4] - 5.83026e-11)/0.971169;
   input5 = (input[5] - -1.87829e-10)/0.963764;
   input6 = (input[6] - 1.06075e-10)/0.902977;
   switch(index) {
     case 0:
         return neuron0x37e41b0();
     default:
         return 0.;
   }
}

double TMVAClassification_TMlpANN::neuron0x37de6d0() {
   return input0;
}

double TMVAClassification_TMlpANN::neuron0x37dea10() {
   return input1;
}

double TMVAClassification_TMlpANN::neuron0x37ded50() {
   return input2;
}

double TMVAClassification_TMlpANN::neuron0x37df090() {
   return input3;
}

double TMVAClassification_TMlpANN::neuron0x37df3d0() {
   return input4;
}

double TMVAClassification_TMlpANN::neuron0x37df710() {
   return input5;
}

double TMVAClassification_TMlpANN::neuron0x37dfa50() {
   return input6;
}

double TMVAClassification_TMlpANN::input0x37dfef0() {
   double input = 0.122866;
   input += synapse0x39a2cb0();
   input += synapse0x39a2840();
   input += synapse0x37e01a0();
   input += synapse0x37e01e0();
   input += synapse0x37e0220();
   input += synapse0x37e0260();
   input += synapse0x37e02a0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37dfef0() {
   double input = input0x37dfef0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e02e0() {
   double input = -4.05423;
   input += synapse0x37e0620();
   input += synapse0x37e0660();
   input += synapse0x37e06a0();
   input += synapse0x37e06e0();
   input += synapse0x37e0720();
   input += synapse0x37e0760();
   input += synapse0x37e07a0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e02e0() {
   double input = input0x37e02e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e07e0() {
   double input = 0.887981;
   input += synapse0x37e0b20();
   input += synapse0x37e0b60();
   input += synapse0x37e0ba0();
   input += synapse0x39a4340();
   input += synapse0x39a4380();
   input += synapse0x37e0cf0();
   input += synapse0x37e0d30();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e07e0() {
   double input = input0x37e07e0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e0d70() {
   double input = 1.03204;
   input += synapse0x37e10b0();
   input += synapse0x37e10f0();
   input += synapse0x37e1130();
   input += synapse0x37e1170();
   input += synapse0x37e11b0();
   input += synapse0x37e11f0();
   input += synapse0x37e1230();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e0d70() {
   double input = input0x37e0d70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e1270() {
   double input = 0.682407;
   input += synapse0x37e15b0();
   input += synapse0x37e15f0();
   input += synapse0x37e1630();
   input += synapse0x37e1670();
   input += synapse0x37e16b0();
   input += synapse0x37e0be0();
   input += synapse0x37e0c20();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e1270() {
   double input = input0x37e1270();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e1900() {
   double input = 0.00438193;
   input += synapse0x37e1bb0();
   input += synapse0x37e1bf0();
   input += synapse0x37e1c30();
   input += synapse0x37e1c70();
   input += synapse0x37e1cb0();
   input += synapse0x37e1cf0();
   input += synapse0x37e1d30();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e1900() {
   double input = input0x37e1900();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e1d70() {
   double input = 0.701016;
   input += synapse0x37e20b0();
   input += synapse0x37e20f0();
   input += synapse0x37e2130();
   input += synapse0x37e2170();
   input += synapse0x37e21b0();
   input += synapse0x37e21f0();
   input += synapse0x37e2230();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e1d70() {
   double input = input0x37e1d70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e2270() {
   double input = -0.391455;
   input += synapse0x37e25b0();
   input += synapse0x37e25f0();
   input += synapse0x37e2630();
   input += synapse0x37e2670();
   input += synapse0x37e26b0();
   input += synapse0x37e26f0();
   input += synapse0x37e2730();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e2270() {
   double input = input0x37e2270();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e2770() {
   double input = -0.74489;
   input += synapse0x37e2ab0();
   input += synapse0x37e2af0();
   input += synapse0x37e2b30();
   input += synapse0x37e2b70();
   input += synapse0x37e2bb0();
   input += synapse0x37e2bf0();
   input += synapse0x37e2c30();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e2770() {
   double input = input0x37e2770();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e2c70() {
   double input = -1.3235;
   input += synapse0x39a4190();
   input += synapse0x39a41d0();
   input += synapse0x37c5e60();
   input += synapse0x39a4630();
   input += synapse0x39a4710();
   input += synapse0x39a4750();
   input += synapse0x39a4790();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e2c70() {
   double input = input0x37e2c70();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e16f0() {
   double input = 2.22931;
   input += synapse0x37e35f0();
   input += synapse0x37e3630();
   input += synapse0x37e3670();
   input += synapse0x37e36b0();
   input += synapse0x37e36f0();
   input += synapse0x37e3730();
   input += synapse0x37e3770();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e16f0() {
   double input = input0x37e16f0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e37b0() {
   double input = 0.670275;
   input += synapse0x37e3af0();
   input += synapse0x37e3b30();
   input += synapse0x37e3b70();
   input += synapse0x37e3bb0();
   input += synapse0x37e3bf0();
   input += synapse0x37e3c30();
   input += synapse0x37e3c70();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e37b0() {
   double input = input0x37e37b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e3cb0() {
   double input = -0.290967;
   input += synapse0x37e3ff0();
   input += synapse0x37e4030();
   input += synapse0x37e4070();
   input += synapse0x37e40b0();
   input += synapse0x37e40f0();
   input += synapse0x37e4130();
   input += synapse0x37e4170();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e3cb0() {
   double input = input0x37e3cb0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x37e41b0() {
   double input = -0.654474;
   input += synapse0x37e44f0();
   input += synapse0x37e4530();
   input += synapse0x37e4570();
   input += synapse0x37e45b0();
   input += synapse0x37e45f0();
   input += synapse0x37e4630();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x37e41b0() {
   double input = input0x37e41b0();
   return (input * 1)+0;
}

double TMVAClassification_TMlpANN::synapse0x39a2cb0() {
   return (neuron0x37de6d0()*0.495388);
}

double TMVAClassification_TMlpANN::synapse0x39a2840() {
   return (neuron0x37dea10()*-0.78039);
}

double TMVAClassification_TMlpANN::synapse0x37e01a0() {
   return (neuron0x37ded50()*-1.866);
}

double TMVAClassification_TMlpANN::synapse0x37e01e0() {
   return (neuron0x37df090()*0.524702);
}

double TMVAClassification_TMlpANN::synapse0x37e0220() {
   return (neuron0x37df3d0()*-1.85744);
}

double TMVAClassification_TMlpANN::synapse0x37e0260() {
   return (neuron0x37df710()*-0.124259);
}

double TMVAClassification_TMlpANN::synapse0x37e02a0() {
   return (neuron0x37dfa50()*2.4443);
}

double TMVAClassification_TMlpANN::synapse0x37e0620() {
   return (neuron0x37de6d0()*-2.04172);
}

double TMVAClassification_TMlpANN::synapse0x37e0660() {
   return (neuron0x37dea10()*-1.07824);
}

double TMVAClassification_TMlpANN::synapse0x37e06a0() {
   return (neuron0x37ded50()*-1.17235);
}

double TMVAClassification_TMlpANN::synapse0x37e06e0() {
   return (neuron0x37df090()*-0.341882);
}

double TMVAClassification_TMlpANN::synapse0x37e0720() {
   return (neuron0x37df3d0()*-1.10447);
}

double TMVAClassification_TMlpANN::synapse0x37e0760() {
   return (neuron0x37df710()*1.22496);
}

double TMVAClassification_TMlpANN::synapse0x37e07a0() {
   return (neuron0x37dfa50()*3.36184);
}

double TMVAClassification_TMlpANN::synapse0x37e0b20() {
   return (neuron0x37de6d0()*-3.64767);
}

double TMVAClassification_TMlpANN::synapse0x37e0b60() {
   return (neuron0x37dea10()*-4.51111);
}

double TMVAClassification_TMlpANN::synapse0x37e0ba0() {
   return (neuron0x37ded50()*-0.672267);
}

double TMVAClassification_TMlpANN::synapse0x39a4340() {
   return (neuron0x37df090()*-0.733359);
}

double TMVAClassification_TMlpANN::synapse0x39a4380() {
   return (neuron0x37df3d0()*-0.587584);
}

double TMVAClassification_TMlpANN::synapse0x37e0cf0() {
   return (neuron0x37df710()*-0.730363);
}

double TMVAClassification_TMlpANN::synapse0x37e0d30() {
   return (neuron0x37dfa50()*3.33162);
}

double TMVAClassification_TMlpANN::synapse0x37e10b0() {
   return (neuron0x37de6d0()*-3.37345);
}

double TMVAClassification_TMlpANN::synapse0x37e10f0() {
   return (neuron0x37dea10()*3.04731);
}

double TMVAClassification_TMlpANN::synapse0x37e1130() {
   return (neuron0x37ded50()*1.8766);
}

double TMVAClassification_TMlpANN::synapse0x37e1170() {
   return (neuron0x37df090()*-0.582154);
}

double TMVAClassification_TMlpANN::synapse0x37e11b0() {
   return (neuron0x37df3d0()*0.920616);
}

double TMVAClassification_TMlpANN::synapse0x37e11f0() {
   return (neuron0x37df710()*-0.535299);
}

double TMVAClassification_TMlpANN::synapse0x37e1230() {
   return (neuron0x37dfa50()*-1.04707);
}

double TMVAClassification_TMlpANN::synapse0x37e15b0() {
   return (neuron0x37de6d0()*-1.73491);
}

double TMVAClassification_TMlpANN::synapse0x37e15f0() {
   return (neuron0x37dea10()*3.04795);
}

double TMVAClassification_TMlpANN::synapse0x37e1630() {
   return (neuron0x37ded50()*-1.58963);
}

double TMVAClassification_TMlpANN::synapse0x37e1670() {
   return (neuron0x37df090()*0.633595);
}

double TMVAClassification_TMlpANN::synapse0x37e16b0() {
   return (neuron0x37df3d0()*-0.0916026);
}

double TMVAClassification_TMlpANN::synapse0x37e0be0() {
   return (neuron0x37df710()*-1.16806);
}

double TMVAClassification_TMlpANN::synapse0x37e0c20() {
   return (neuron0x37dfa50()*-0.989015);
}

double TMVAClassification_TMlpANN::synapse0x37e1bb0() {
   return (neuron0x37de6d0()*1.7483);
}

double TMVAClassification_TMlpANN::synapse0x37e1bf0() {
   return (neuron0x37dea10()*1.39767);
}

double TMVAClassification_TMlpANN::synapse0x37e1c30() {
   return (neuron0x37ded50()*-0.779216);
}

double TMVAClassification_TMlpANN::synapse0x37e1c70() {
   return (neuron0x37df090()*0.758002);
}

double TMVAClassification_TMlpANN::synapse0x37e1cb0() {
   return (neuron0x37df3d0()*0.38137);
}

double TMVAClassification_TMlpANN::synapse0x37e1cf0() {
   return (neuron0x37df710()*0.897103);
}

double TMVAClassification_TMlpANN::synapse0x37e1d30() {
   return (neuron0x37dfa50()*-1.66929);
}

double TMVAClassification_TMlpANN::synapse0x37e20b0() {
   return (neuron0x37de6d0()*-2.17441);
}

double TMVAClassification_TMlpANN::synapse0x37e20f0() {
   return (neuron0x37dea10()*4.28242);
}

double TMVAClassification_TMlpANN::synapse0x37e2130() {
   return (neuron0x37ded50()*-0.322545);
}

double TMVAClassification_TMlpANN::synapse0x37e2170() {
   return (neuron0x37df090()*-0.100458);
}

double TMVAClassification_TMlpANN::synapse0x37e21b0() {
   return (neuron0x37df3d0()*-1.45871);
}

double TMVAClassification_TMlpANN::synapse0x37e21f0() {
   return (neuron0x37df710()*0.689424);
}

double TMVAClassification_TMlpANN::synapse0x37e2230() {
   return (neuron0x37dfa50()*1.6857);
}

double TMVAClassification_TMlpANN::synapse0x37e25b0() {
   return (neuron0x37dfef0()*-0.942926);
}

double TMVAClassification_TMlpANN::synapse0x37e25f0() {
   return (neuron0x37e02e0()*4.01968);
}

double TMVAClassification_TMlpANN::synapse0x37e2630() {
   return (neuron0x37e07e0()*-1.93894);
}

double TMVAClassification_TMlpANN::synapse0x37e2670() {
   return (neuron0x37e0d70()*-0.239578);
}

double TMVAClassification_TMlpANN::synapse0x37e26b0() {
   return (neuron0x37e1270()*-0.293806);
}

double TMVAClassification_TMlpANN::synapse0x37e26f0() {
   return (neuron0x37e1900()*-3.92059);
}

double TMVAClassification_TMlpANN::synapse0x37e2730() {
   return (neuron0x37e1d70()*2.46345);
}

double TMVAClassification_TMlpANN::synapse0x37e2ab0() {
   return (neuron0x37dfef0()*1.35251);
}

double TMVAClassification_TMlpANN::synapse0x37e2af0() {
   return (neuron0x37e02e0()*-2.86588);
}

double TMVAClassification_TMlpANN::synapse0x37e2b30() {
   return (neuron0x37e07e0()*0.233539);
}

double TMVAClassification_TMlpANN::synapse0x37e2b70() {
   return (neuron0x37e0d70()*0.299034);
}

double TMVAClassification_TMlpANN::synapse0x37e2bb0() {
   return (neuron0x37e1270()*0.28121);
}

double TMVAClassification_TMlpANN::synapse0x37e2bf0() {
   return (neuron0x37e1900()*0.246862);
}

double TMVAClassification_TMlpANN::synapse0x37e2c30() {
   return (neuron0x37e1d70()*-2.65052);
}

double TMVAClassification_TMlpANN::synapse0x39a4190() {
   return (neuron0x37dfef0()*-0.370321);
}

double TMVAClassification_TMlpANN::synapse0x39a41d0() {
   return (neuron0x37e02e0()*-0.00671522);
}

double TMVAClassification_TMlpANN::synapse0x37c5e60() {
   return (neuron0x37e07e0()*0.176516);
}

double TMVAClassification_TMlpANN::synapse0x39a4630() {
   return (neuron0x37e0d70()*0.0778874);
}

double TMVAClassification_TMlpANN::synapse0x39a4710() {
   return (neuron0x37e1270()*1.54815);
}

double TMVAClassification_TMlpANN::synapse0x39a4750() {
   return (neuron0x37e1900()*-0.781885);
}

double TMVAClassification_TMlpANN::synapse0x39a4790() {
   return (neuron0x37e1d70()*1.07873);
}

double TMVAClassification_TMlpANN::synapse0x37e35f0() {
   return (neuron0x37dfef0()*-1.27203);
}

double TMVAClassification_TMlpANN::synapse0x37e3630() {
   return (neuron0x37e02e0()*5.23738);
}

double TMVAClassification_TMlpANN::synapse0x37e3670() {
   return (neuron0x37e07e0()*-2.76234);
}

double TMVAClassification_TMlpANN::synapse0x37e36b0() {
   return (neuron0x37e0d70()*-1.13343);
}

double TMVAClassification_TMlpANN::synapse0x37e36f0() {
   return (neuron0x37e1270()*-1.21647);
}

double TMVAClassification_TMlpANN::synapse0x37e3730() {
   return (neuron0x37e1900()*-2.91627);
}

double TMVAClassification_TMlpANN::synapse0x37e3770() {
   return (neuron0x37e1d70()*2.3314);
}

double TMVAClassification_TMlpANN::synapse0x37e3af0() {
   return (neuron0x37dfef0()*-0.296663);
}

double TMVAClassification_TMlpANN::synapse0x37e3b30() {
   return (neuron0x37e02e0()*0.895533);
}

double TMVAClassification_TMlpANN::synapse0x37e3b70() {
   return (neuron0x37e07e0()*0.071661);
}

double TMVAClassification_TMlpANN::synapse0x37e3bb0() {
   return (neuron0x37e0d70()*0.776566);
}

double TMVAClassification_TMlpANN::synapse0x37e3bf0() {
   return (neuron0x37e1270()*0.214147);
}

double TMVAClassification_TMlpANN::synapse0x37e3c30() {
   return (neuron0x37e1900()*-1.07352);
}

double TMVAClassification_TMlpANN::synapse0x37e3c70() {
   return (neuron0x37e1d70()*0.0956986);
}

double TMVAClassification_TMlpANN::synapse0x37e3ff0() {
   return (neuron0x37dfef0()*0.0903635);
}

double TMVAClassification_TMlpANN::synapse0x37e4030() {
   return (neuron0x37e02e0()*2.18015);
}

double TMVAClassification_TMlpANN::synapse0x37e4070() {
   return (neuron0x37e07e0()*-0.742629);
}

double TMVAClassification_TMlpANN::synapse0x37e40b0() {
   return (neuron0x37e0d70()*-0.298821);
}

double TMVAClassification_TMlpANN::synapse0x37e40f0() {
   return (neuron0x37e1270()*0.24501);
}

double TMVAClassification_TMlpANN::synapse0x37e4130() {
   return (neuron0x37e1900()*-1.24722);
}

double TMVAClassification_TMlpANN::synapse0x37e4170() {
   return (neuron0x37e1d70()*2.53706);
}

double TMVAClassification_TMlpANN::synapse0x37e44f0() {
   return (neuron0x37e2270()*-1.21023);
}

double TMVAClassification_TMlpANN::synapse0x37e4530() {
   return (neuron0x37e2770()*1.9951);
}

double TMVAClassification_TMlpANN::synapse0x37e4570() {
   return (neuron0x37e2c70()*2.35378);
}

double TMVAClassification_TMlpANN::synapse0x37e45b0() {
   return (neuron0x37e16f0()*3.18481);
}

double TMVAClassification_TMlpANN::synapse0x37e45f0() {
   return (neuron0x37e37b0()*-0.599528);
}

double TMVAClassification_TMlpANN::synapse0x37e4630() {
   return (neuron0x37e3cb0()*-1.78214);
}

