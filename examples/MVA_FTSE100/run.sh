#!/bin/bash

./../../build/examples/MakeIndicatorTuple/maketuple --spx_file "../../db/FTSE100.csv" --output_file "outputFile_FTSE100.root" --begin_date "1999-01-01" --end_date "2006-01-01" --day_shift "7"
./../../build/examples/MVAAnalysis/mva --input_file "outputFile_FTSE100.root" --tree_path "data" --output_file "outputFile_FTSE100_withMVA.root"
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/FTSE100.csv" --begin_date "2006-02-01" --end_date "2010-11-01" --mva_type MLP --cut_value 0.72  --day_shift "7"

#./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/FTSE100.csv" --begin_date "2006-02-01" --end_date "2010-11-01" --mva_type MLPBFGS --cut_value 0.3 | tee stdout_FTSE100_010206-011110_MLP_0.3.txt


#./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/FTSE100.csv" --begin_date "2006-02-01" --end_date "2010-11-01" --mva_type TMlpANN --cut_value 0.7105 --day_shift "7" | tee stdout_FTSE100_010206-011110_TMlpANN_0.763.txt
#./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/FTSE100.csv" --begin_date "2006-02-01" --end_date "2010-11-01" --mva_type TMlpANN --cut_value 0.7107 --day_shift "7" | tee stdout_FTSE100_010206-011110_TMlpANN_0.7107.txt

#./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/FTSE100.csv" --begin_date "2006-02-01" --end_date "2010-11-01" --mva_type "TMlpANN" --cut_value "0.741" --day_shift "9"



