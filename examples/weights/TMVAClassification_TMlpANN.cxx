#include "weights/TMVAClassification_TMlpANN.h"
#include <cmath>

double TMVAClassification_TMlpANN::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5,double in6) {
   input0 = (in0 - -7.54451e-10)/1.57974;
   input1 = (in1 - 6.04872e-10)/1.08947;
   input2 = (in2 - -6.69504e-10)/1.03898;
   input3 = (in3 - -1.23583e-09)/1.00998;
   input4 = (in4 - 7.52717e-11)/0.849648;
   input5 = (in5 - -4.16883e-10)/0.749825;
   input6 = (in6 - 9.51678e-11)/0.0831555;
   switch(index) {
     case 0:
         return neuron0x22139d0();
     default:
         return 0.;
   }
}

double TMVAClassification_TMlpANN::Value(int index, double* input) {
   input0 = (input[0] - -7.54451e-10)/1.57974;
   input1 = (input[1] - 6.04872e-10)/1.08947;
   input2 = (input[2] - -6.69504e-10)/1.03898;
   input3 = (input[3] - -1.23583e-09)/1.00998;
   input4 = (input[4] - 7.52717e-11)/0.849648;
   input5 = (input[5] - -4.16883e-10)/0.749825;
   input6 = (input[6] - 9.51678e-11)/0.0831555;
   switch(index) {
     case 0:
         return neuron0x22139d0();
     default:
         return 0.;
   }
}

double TMVAClassification_TMlpANN::neuron0x302df30() {
   return input0;
}

double TMVAClassification_TMlpANN::neuron0x21f8670() {
   return input1;
}

double TMVAClassification_TMlpANN::neuron0x21ed180() {
   return input2;
}

double TMVAClassification_TMlpANN::neuron0x2215ef0() {
   return input3;
}

double TMVAClassification_TMlpANN::neuron0x2f572f0() {
   return input4;
}

double TMVAClassification_TMlpANN::neuron0x2f50670() {
   return input5;
}

double TMVAClassification_TMlpANN::neuron0x2229920() {
   return input6;
}

double TMVAClassification_TMlpANN::input0x2243e80() {
   double input = -1.2803;
   input += synapse0x21ecf40();
   input += synapse0x21ef4d0();
   input += synapse0x22504f0();
   input += synapse0x21f7570();
   input += synapse0x21fc820();
   input += synapse0x2242d70();
   input += synapse0x2244130();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2243e80() {
   double input = input0x2243e80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x220d670() {
   double input = -3.3865;
   input += synapse0x220d9b0();
   input += synapse0x220d9f0();
   input += synapse0x220da30();
   input += synapse0x220da70();
   input += synapse0x2209af0();
   input += synapse0x2209b30();
   input += synapse0x2209a40();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x220d670() {
   double input = input0x220d670();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x21f0600() {
   double input = 3.31314;
   input += synapse0x2209a80();
   input += synapse0x21f0940();
   input += synapse0x21f0980();
   input += synapse0x2244280();
   input += synapse0x2209990();
   input += synapse0x22099d0();
   input += synapse0x22098e0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x21f0600() {
   double input = input0x21f0600();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x2214fa0() {
   double input = 0.224754;
   input += synapse0x2209920();
   input += synapse0x2215250();
   input += synapse0x2215290();
   input += synapse0x22152d0();
   input += synapse0x2215310();
   input += synapse0x2215350();
   input += synapse0x2215390();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2214fa0() {
   double input = input0x2214fa0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x2216e10() {
   double input = 0.857805;
   input += synapse0x2217150();
   input += synapse0x2217190();
   input += synapse0x22171d0();
   input += synapse0x2217210();
   input += synapse0x2209830();
   input += synapse0x2209870();
   input += synapse0x2f2f930();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2216e10() {
   double input = input0x2216e10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x2db6d20() {
   double input = -2.04507;
   input += synapse0x2f2f970();
   input += synapse0x21f8a40();
   input += synapse0x302e300();
   input += synapse0x222ad20();
   input += synapse0x222ad60();
   input += synapse0x2264a00();
   input += synapse0x2264a40();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2db6d20() {
   double input = input0x2db6d20();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x2f79880() {
   double input = 2.36988;
   input += synapse0x2f79bc0();
   input += synapse0x2f79c00();
   input += synapse0x2f79c40();
   input += synapse0x2f79c80();
   input += synapse0x220a120();
   input += synapse0x220a160();
   input += synapse0x2209fc0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2f79880() {
   double input = input0x2f79880();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x21ec150() {
   double input = 3.45462;
   input += synapse0x220a000();
   input += synapse0x21ec490();
   input += synapse0x21ec4d0();
   input += synapse0x21ec510();
   input += synapse0x21ec550();
   input += synapse0x21f7ac0();
   input += synapse0x21f7b00();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x21ec150() {
   double input = input0x21ec150();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x2f381a0() {
   double input = 3.21547;
   input += synapse0x2f384e0();
   input += synapse0x2f38520();
   input += synapse0x2f38560();
   input += synapse0x2f385a0();
   input += synapse0x3088930();
   input += synapse0x3088970();
   input += synapse0x2209e60();
   input += synapse0x2209ea0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2f381a0() {
   double input = input0x2f381a0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x22452b0() {
   double input = -1.17748;
   input += synapse0x2219060();
   input += synapse0x22190a0();
   input += synapse0x2f50f40();
   input += synapse0x2209c50();
   input += synapse0x2236e50();
   input += synapse0x2236e90();
   input += synapse0x21f09c0();
   input += synapse0x21f0a00();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x22452b0() {
   double input = input0x22452b0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x2db6b10() {
   double input = -1.21895;
   input += synapse0x21ed550();
   input += synapse0x220a1d0();
   input += synapse0x220a210();
   input += synapse0x2db6ca0();
   input += synapse0x2db6ce0();
   input += synapse0x2216230();
   input += synapse0x2216270();
   input += synapse0x22162b0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2db6b10() {
   double input = input0x2db6b10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x2f4ad00() {
   double input = -2.49577;
   input += synapse0x22162f0();
   input += synapse0x2f4b040();
   input += synapse0x2f4b080();
   input += synapse0x2f4b0c0();
   input += synapse0x2f4b100();
   input += synapse0x2f57630();
   input += synapse0x2f57670();
   input += synapse0x2f576b0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2f4ad00() {
   double input = input0x2f4ad00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x2f48c40() {
   double input = 1.14467;
   input += synapse0x2f576f0();
   input += synapse0x2f48f80();
   input += synapse0x2f48fc0();
   input += synapse0x2f49000();
   input += synapse0x2f49040();
   input += synapse0x2f509b0();
   input += synapse0x2f509f0();
   input += synapse0x2f50a30();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2f48c40() {
   double input = input0x2f48c40();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x2f40ee0() {
   double input = -0.541528;
   input += synapse0x2f50a70();
   input += synapse0x2f41220();
   input += synapse0x2f41260();
   input += synapse0x2f412a0();
   input += synapse0x2f412e0();
   input += synapse0x2229c60();
   input += synapse0x2229ca0();
   input += synapse0x2229ce0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2f40ee0() {
   double input = input0x2f40ee0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x2f51080() {
   double input = -0.735807;
   input += synapse0x2229d20();
   input += synapse0x2f513c0();
   input += synapse0x2f51400();
   input += synapse0x2f51440();
   input += synapse0x2f51480();
   input += synapse0x22455f0();
   input += synapse0x2245630();
   input += synapse0x2245670();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x2f51080() {
   double input = input0x2f51080();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x22139d0() {
   double input = -2.85608;
   input += synapse0x2213d10();
   input += synapse0x2213d50();
   input += synapse0x2213d90();
   input += synapse0x22456b0();
   input += synapse0x2213dd0();
   input += synapse0x2db68c0();
   input += synapse0x2db6900();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x22139d0() {
   double input = input0x22139d0();
   return (input * 1)+0;
}

double TMVAClassification_TMlpANN::synapse0x21ecf40() {
   return (neuron0x302df30()*3.15348);
}

double TMVAClassification_TMlpANN::synapse0x21ef4d0() {
   return (neuron0x21f8670()*2.89845);
}

double TMVAClassification_TMlpANN::synapse0x22504f0() {
   return (neuron0x21ed180()*0.758922);
}

double TMVAClassification_TMlpANN::synapse0x21f7570() {
   return (neuron0x2215ef0()*-3.18398);
}

double TMVAClassification_TMlpANN::synapse0x21fc820() {
   return (neuron0x2f572f0()*1.95476);
}

double TMVAClassification_TMlpANN::synapse0x2242d70() {
   return (neuron0x2f50670()*2.34512);
}

double TMVAClassification_TMlpANN::synapse0x2244130() {
   return (neuron0x2229920()*-0.0693313);
}

double TMVAClassification_TMlpANN::synapse0x220d9b0() {
   return (neuron0x302df30()*0.0630439);
}

double TMVAClassification_TMlpANN::synapse0x220d9f0() {
   return (neuron0x21f8670()*1.12119);
}

double TMVAClassification_TMlpANN::synapse0x220da30() {
   return (neuron0x21ed180()*-2.08907);
}

double TMVAClassification_TMlpANN::synapse0x220da70() {
   return (neuron0x2215ef0()*-1.40906);
}

double TMVAClassification_TMlpANN::synapse0x2209af0() {
   return (neuron0x2f572f0()*2.69773);
}

double TMVAClassification_TMlpANN::synapse0x2209b30() {
   return (neuron0x2f50670()*-0.793678);
}

double TMVAClassification_TMlpANN::synapse0x2209a40() {
   return (neuron0x2229920()*-0.331304);
}

double TMVAClassification_TMlpANN::synapse0x2209a80() {
   return (neuron0x302df30()*-1.37229);
}

double TMVAClassification_TMlpANN::synapse0x21f0940() {
   return (neuron0x21f8670()*0.0786812);
}

double TMVAClassification_TMlpANN::synapse0x21f0980() {
   return (neuron0x21ed180()*0.0729431);
}

double TMVAClassification_TMlpANN::synapse0x2244280() {
   return (neuron0x2215ef0()*1.42665);
}

double TMVAClassification_TMlpANN::synapse0x2209990() {
   return (neuron0x2f572f0()*0.866365);
}

double TMVAClassification_TMlpANN::synapse0x22099d0() {
   return (neuron0x2f50670()*1.52442);
}

double TMVAClassification_TMlpANN::synapse0x22098e0() {
   return (neuron0x2229920()*-2.60472);
}

double TMVAClassification_TMlpANN::synapse0x2209920() {
   return (neuron0x302df30()*-1.54894);
}

double TMVAClassification_TMlpANN::synapse0x2215250() {
   return (neuron0x21f8670()*-1.48592);
}

double TMVAClassification_TMlpANN::synapse0x2215290() {
   return (neuron0x21ed180()*-1.29301);
}

double TMVAClassification_TMlpANN::synapse0x22152d0() {
   return (neuron0x2215ef0()*4.25182);
}

double TMVAClassification_TMlpANN::synapse0x2215310() {
   return (neuron0x2f572f0()*-0.271816);
}

double TMVAClassification_TMlpANN::synapse0x2215350() {
   return (neuron0x2f50670()*1.11445);
}

double TMVAClassification_TMlpANN::synapse0x2215390() {
   return (neuron0x2229920()*0.0834062);
}

double TMVAClassification_TMlpANN::synapse0x2217150() {
   return (neuron0x302df30()*1.69628);
}

double TMVAClassification_TMlpANN::synapse0x2217190() {
   return (neuron0x21f8670()*-3.42162);
}

double TMVAClassification_TMlpANN::synapse0x22171d0() {
   return (neuron0x21ed180()*1.80191);
}

double TMVAClassification_TMlpANN::synapse0x2217210() {
   return (neuron0x2215ef0()*4.92717);
}

double TMVAClassification_TMlpANN::synapse0x2209830() {
   return (neuron0x2f572f0()*-0.0152899);
}

double TMVAClassification_TMlpANN::synapse0x2209870() {
   return (neuron0x2f50670()*0.751297);
}

double TMVAClassification_TMlpANN::synapse0x2f2f930() {
   return (neuron0x2229920()*-0.099092);
}

double TMVAClassification_TMlpANN::synapse0x2f2f970() {
   return (neuron0x302df30()*-1.01779);
}

double TMVAClassification_TMlpANN::synapse0x21f8a40() {
   return (neuron0x21f8670()*2.77201);
}

double TMVAClassification_TMlpANN::synapse0x302e300() {
   return (neuron0x21ed180()*-2.1613);
}

double TMVAClassification_TMlpANN::synapse0x222ad20() {
   return (neuron0x2215ef0()*3.03525);
}

double TMVAClassification_TMlpANN::synapse0x222ad60() {
   return (neuron0x2f572f0()*-2.64216);
}

double TMVAClassification_TMlpANN::synapse0x2264a00() {
   return (neuron0x2f50670()*2.85006);
}

double TMVAClassification_TMlpANN::synapse0x2264a40() {
   return (neuron0x2229920()*-0.567183);
}

double TMVAClassification_TMlpANN::synapse0x2f79bc0() {
   return (neuron0x302df30()*-2.95473);
}

double TMVAClassification_TMlpANN::synapse0x2f79c00() {
   return (neuron0x21f8670()*0.461404);
}

double TMVAClassification_TMlpANN::synapse0x2f79c40() {
   return (neuron0x21ed180()*-2.53791);
}

double TMVAClassification_TMlpANN::synapse0x2f79c80() {
   return (neuron0x2215ef0()*-3.1024);
}

double TMVAClassification_TMlpANN::synapse0x220a120() {
   return (neuron0x2f572f0()*0.836658);
}

double TMVAClassification_TMlpANN::synapse0x220a160() {
   return (neuron0x2f50670()*-1.06114);
}

double TMVAClassification_TMlpANN::synapse0x2209fc0() {
   return (neuron0x2229920()*-0.917541);
}

double TMVAClassification_TMlpANN::synapse0x220a000() {
   return (neuron0x302df30()*3.72148);
}

double TMVAClassification_TMlpANN::synapse0x21ec490() {
   return (neuron0x21f8670()*-0.147279);
}

double TMVAClassification_TMlpANN::synapse0x21ec4d0() {
   return (neuron0x21ed180()*0.260981);
}

double TMVAClassification_TMlpANN::synapse0x21ec510() {
   return (neuron0x2215ef0()*2.32145);
}

double TMVAClassification_TMlpANN::synapse0x21ec550() {
   return (neuron0x2f572f0()*-1.63356);
}

double TMVAClassification_TMlpANN::synapse0x21f7ac0() {
   return (neuron0x2f50670()*-1.09358);
}

double TMVAClassification_TMlpANN::synapse0x21f7b00() {
   return (neuron0x2229920()*-1.0247);
}

double TMVAClassification_TMlpANN::synapse0x2f384e0() {
   return (neuron0x2243e80()*-2.55463);
}

double TMVAClassification_TMlpANN::synapse0x2f38520() {
   return (neuron0x220d670()*4.63368);
}

double TMVAClassification_TMlpANN::synapse0x2f38560() {
   return (neuron0x21f0600()*0.0298108);
}

double TMVAClassification_TMlpANN::synapse0x2f385a0() {
   return (neuron0x2214fa0()*-2.85078);
}

double TMVAClassification_TMlpANN::synapse0x3088930() {
   return (neuron0x2216e10()*1.85883);
}

double TMVAClassification_TMlpANN::synapse0x3088970() {
   return (neuron0x2db6d20()*2.80523);
}

double TMVAClassification_TMlpANN::synapse0x2209e60() {
   return (neuron0x2f79880()*-0.199413);
}

double TMVAClassification_TMlpANN::synapse0x2209ea0() {
   return (neuron0x21ec150()*-1.51068);
}

double TMVAClassification_TMlpANN::synapse0x2219060() {
   return (neuron0x2243e80()*0.209318);
}

double TMVAClassification_TMlpANN::synapse0x22190a0() {
   return (neuron0x220d670()*-1.16025);
}

double TMVAClassification_TMlpANN::synapse0x2f50f40() {
   return (neuron0x21f0600()*0.207941);
}

double TMVAClassification_TMlpANN::synapse0x2209c50() {
   return (neuron0x2214fa0()*-0.845886);
}

double TMVAClassification_TMlpANN::synapse0x2236e50() {
   return (neuron0x2216e10()*-1.25554);
}

double TMVAClassification_TMlpANN::synapse0x2236e90() {
   return (neuron0x2db6d20()*-1.78543);
}

double TMVAClassification_TMlpANN::synapse0x21f09c0() {
   return (neuron0x2f79880()*-0.289264);
}

double TMVAClassification_TMlpANN::synapse0x21f0a00() {
   return (neuron0x21ec150()*0.289277);
}

double TMVAClassification_TMlpANN::synapse0x21ed550() {
   return (neuron0x2243e80()*-0.169188);
}

double TMVAClassification_TMlpANN::synapse0x220a1d0() {
   return (neuron0x220d670()*-0.896722);
}

double TMVAClassification_TMlpANN::synapse0x220a210() {
   return (neuron0x21f0600()*1.54736);
}

double TMVAClassification_TMlpANN::synapse0x2db6ca0() {
   return (neuron0x2214fa0()*-0.307861);
}

double TMVAClassification_TMlpANN::synapse0x2db6ce0() {
   return (neuron0x2216e10()*4.00117);
}

double TMVAClassification_TMlpANN::synapse0x2216230() {
   return (neuron0x2db6d20()*-0.738139);
}

double TMVAClassification_TMlpANN::synapse0x2216270() {
   return (neuron0x2f79880()*-0.0796091);
}

double TMVAClassification_TMlpANN::synapse0x22162b0() {
   return (neuron0x21ec150()*-0.147387);
}

double TMVAClassification_TMlpANN::synapse0x22162f0() {
   return (neuron0x2243e80()*3.71494);
}

double TMVAClassification_TMlpANN::synapse0x2f4b040() {
   return (neuron0x220d670()*0.370427);
}

double TMVAClassification_TMlpANN::synapse0x2f4b080() {
   return (neuron0x21f0600()*-1.81797);
}

double TMVAClassification_TMlpANN::synapse0x2f4b0c0() {
   return (neuron0x2214fa0()*0.669209);
}

double TMVAClassification_TMlpANN::synapse0x2f4b100() {
   return (neuron0x2216e10()*3.76085);
}

double TMVAClassification_TMlpANN::synapse0x2f57630() {
   return (neuron0x2db6d20()*0.555033);
}

double TMVAClassification_TMlpANN::synapse0x2f57670() {
   return (neuron0x2f79880()*2.88233);
}

double TMVAClassification_TMlpANN::synapse0x2f576b0() {
   return (neuron0x21ec150()*-0.45831);
}

double TMVAClassification_TMlpANN::synapse0x2f576f0() {
   return (neuron0x2243e80()*-1.24929);
}

double TMVAClassification_TMlpANN::synapse0x2f48f80() {
   return (neuron0x220d670()*-0.604139);
}

double TMVAClassification_TMlpANN::synapse0x2f48fc0() {
   return (neuron0x21f0600()*2.40868);
}

double TMVAClassification_TMlpANN::synapse0x2f49000() {
   return (neuron0x2214fa0()*-1.24654);
}

double TMVAClassification_TMlpANN::synapse0x2f49040() {
   return (neuron0x2216e10()*1.75588);
}

double TMVAClassification_TMlpANN::synapse0x2f509b0() {
   return (neuron0x2db6d20()*-0.894038);
}

double TMVAClassification_TMlpANN::synapse0x2f509f0() {
   return (neuron0x2f79880()*-2.38198);
}

double TMVAClassification_TMlpANN::synapse0x2f50a30() {
   return (neuron0x21ec150()*0.0903986);
}

double TMVAClassification_TMlpANN::synapse0x2f50a70() {
   return (neuron0x2243e80()*0.925329);
}

double TMVAClassification_TMlpANN::synapse0x2f41220() {
   return (neuron0x220d670()*1.27782);
}

double TMVAClassification_TMlpANN::synapse0x2f41260() {
   return (neuron0x21f0600()*-0.191033);
}

double TMVAClassification_TMlpANN::synapse0x2f412a0() {
   return (neuron0x2214fa0()*-0.289795);
}

double TMVAClassification_TMlpANN::synapse0x2f412e0() {
   return (neuron0x2216e10()*-1.32562);
}

double TMVAClassification_TMlpANN::synapse0x2229c60() {
   return (neuron0x2db6d20()*0.0657033);
}

double TMVAClassification_TMlpANN::synapse0x2229ca0() {
   return (neuron0x2f79880()*0.0306843);
}

double TMVAClassification_TMlpANN::synapse0x2229ce0() {
   return (neuron0x21ec150()*-0.0769906);
}

double TMVAClassification_TMlpANN::synapse0x2229d20() {
   return (neuron0x2243e80()*-1.0636);
}

double TMVAClassification_TMlpANN::synapse0x2f513c0() {
   return (neuron0x220d670()*-0.257004);
}

double TMVAClassification_TMlpANN::synapse0x2f51400() {
   return (neuron0x21f0600()*0.160907);
}

double TMVAClassification_TMlpANN::synapse0x2f51440() {
   return (neuron0x2214fa0()*-0.10929);
}

double TMVAClassification_TMlpANN::synapse0x2f51480() {
   return (neuron0x2216e10()*-1.22915);
}

double TMVAClassification_TMlpANN::synapse0x22455f0() {
   return (neuron0x2db6d20()*-0.823102);
}

double TMVAClassification_TMlpANN::synapse0x2245630() {
   return (neuron0x2f79880()*-1.43498);
}

double TMVAClassification_TMlpANN::synapse0x2245670() {
   return (neuron0x21ec150()*-0.36991);
}

double TMVAClassification_TMlpANN::synapse0x2213d10() {
   return (neuron0x2f381a0()*2.0118);
}

double TMVAClassification_TMlpANN::synapse0x2213d50() {
   return (neuron0x22452b0()*0.200874);
}

double TMVAClassification_TMlpANN::synapse0x2213d90() {
   return (neuron0x2db6b10()*-3.22257);
}

double TMVAClassification_TMlpANN::synapse0x22456b0() {
   return (neuron0x2f4ad00()*2.97287);
}

double TMVAClassification_TMlpANN::synapse0x2213dd0() {
   return (neuron0x2f48c40()*3.00275);
}

double TMVAClassification_TMlpANN::synapse0x2db68c0() {
   return (neuron0x2f40ee0()*-1.09856);
}

double TMVAClassification_TMlpANN::synapse0x2db6900() {
   return (neuron0x2f51080()*-0.402767);
}

