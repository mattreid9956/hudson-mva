#include "weights/TMVAClassification_TMlpANN.h"
#include <cmath>

double TMVAClassification_TMlpANN::Value(int index,double in0,double in1,double in2,double in3,double in4,double in5) {
   input0 = (in0 - 1.91031e-09)/1.21473;
   input1 = (in1 - 4.99119e-10)/1.02875;
   input2 = (in2 - -1.20999e-09)/0.996959;
   input3 = (in3 - -3.83712e-09)/0.992225;
   input4 = (in4 - -1.75998e-10)/0.929593;
   input5 = (in5 - -2.19997e-10)/0.866828;
   switch(index) {
     case 0:
         return neuron0x25ef180();
     default:
         return 0.;
   }
}

double TMVAClassification_TMlpANN::Value(int index, double* input) {
   input0 = (input[0] - 1.91031e-09)/1.21473;
   input1 = (input[1] - 4.99119e-10)/1.02875;
   input2 = (input[2] - -1.20999e-09)/0.996959;
   input3 = (input[3] - -3.83712e-09)/0.992225;
   input4 = (input[4] - -1.75998e-10)/0.929593;
   input5 = (input[5] - -2.19997e-10)/0.866828;
   switch(index) {
     case 0:
         return neuron0x25ef180();
     default:
         return 0.;
   }
}

double TMVAClassification_TMlpANN::neuron0x25e9c90() {
   return input0;
}

double TMVAClassification_TMlpANN::neuron0x25e9fd0() {
   return input1;
}

double TMVAClassification_TMlpANN::neuron0x25ea310() {
   return input2;
}

double TMVAClassification_TMlpANN::neuron0x25ea650() {
   return input3;
}

double TMVAClassification_TMlpANN::neuron0x25ea990() {
   return input4;
}

double TMVAClassification_TMlpANN::neuron0x25eacd0() {
   return input5;
}

double TMVAClassification_TMlpANN::input0x25eb140() {
   double input = 0.0607981;
   input += synapse0x25eb3f0();
   input += synapse0x25eb430();
   input += synapse0x25eb470();
   input += synapse0x25eb4b0();
   input += synapse0x25eb4f0();
   input += synapse0x25eb530();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25eb140() {
   double input = input0x25eb140();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25eb570() {
   double input = -1.90012;
   input += synapse0x25eb8b0();
   input += synapse0x25eb8f0();
   input += synapse0x25eb930();
   input += synapse0x25eb970();
   input += synapse0x25eb9b0();
   input += synapse0x25eb9f0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25eb570() {
   double input = input0x25eb570();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25eba30() {
   double input = -3.04142;
   input += synapse0x25ebd70();
   input += synapse0x25ebdb0();
   input += synapse0x25ebdf0();
   input += synapse0x25ebe30();
   input += synapse0x25ebe70();
   input += synapse0x2429a70();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25eba30() {
   double input = input0x25eba30();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25ebfc0() {
   double input = 3.31357;
   input += synapse0x2429ab0();
   input += synapse0x25ec300();
   input += synapse0x25ec340();
   input += synapse0x25ec380();
   input += synapse0x25ec3c0();
   input += synapse0x25ec400();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25ebfc0() {
   double input = input0x25ebfc0();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25ec440() {
   double input = -2.02606;
   input += synapse0x25ec780();
   input += synapse0x25ec7c0();
   input += synapse0x25ec800();
   input += synapse0x25ec840();
   input += synapse0x25ec880();
   input += synapse0x25ec8c0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25ec440() {
   double input = input0x25ec440();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25ec900() {
   double input = 1.57327;
   input += synapse0x25ecc40();
   input += synapse0x25ecc80();
   input += synapse0x25eccc0();
   input += synapse0x25ebeb0();
   input += synapse0x25ebef0();
   input += synapse0x25ebf30();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25ec900() {
   double input = input0x25ec900();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25ecf10() {
   double input = -2.00554;
   input += synapse0x25ebf70();
   input += synapse0x25ed250();
   input += synapse0x25ed290();
   input += synapse0x25ed2d0();
   input += synapse0x25ed310();
   input += synapse0x25ed350();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25ecf10() {
   double input = input0x25ecf10();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25ed390() {
   double input = -1.03531;
   input += synapse0x25ed6d0();
   input += synapse0x25ed710();
   input += synapse0x25ed750();
   input += synapse0x25ed790();
   input += synapse0x25ed7d0();
   input += synapse0x25ed810();
   input += synapse0x25ed850();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25ed390() {
   double input = input0x25ed390();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25ed890() {
   double input = 0.581936;
   input += synapse0x25edbd0();
   input += synapse0x25edc10();
   input += synapse0x25edc50();
   input += synapse0x25edc90();
   input += synapse0x25edcd0();
   input += synapse0x25edd10();
   input += synapse0x25edd50();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25ed890() {
   double input = input0x25ed890();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25edd90() {
   double input = 1.78641;
   input += synapse0x25ee0d0();
   input += synapse0x25ee110();
   input += synapse0x25ee150();
   input += synapse0x25ee190();
   input += synapse0x25ee1d0();
   input += synapse0x25ee210();
   input += synapse0x25ee250();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25edd90() {
   double input = input0x25edd90();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25ee290() {
   double input = -1.42172;
   input += synapse0x245ef60();
   input += synapse0x245efa0();
   input += synapse0x242a2f0();
   input += synapse0x25d8f00();
   input += synapse0x25d8f40();
   input += synapse0x25d8f80();
   input += synapse0x2458180();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25ee290() {
   double input = input0x25ee290();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25ecd00() {
   double input = -0.903154;
   input += synapse0x24582e0();
   input += synapse0x25ece90();
   input += synapse0x25eced0();
   input += synapse0x25eeb80();
   input += synapse0x25eebc0();
   input += synapse0x25eec00();
   input += synapse0x25eec40();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25ecd00() {
   double input = input0x25ecd00();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25eec80() {
   double input = -0.701537;
   input += synapse0x25eefc0();
   input += synapse0x25ef000();
   input += synapse0x25ef040();
   input += synapse0x25ef080();
   input += synapse0x25ef0c0();
   input += synapse0x25ef100();
   input += synapse0x25ef140();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25eec80() {
   double input = input0x25eec80();
   return ((input < -709. ? 0. : (1/(1+exp(-input)))) * 1)+0;
}

double TMVAClassification_TMlpANN::input0x25ef180() {
   double input = -1.44211;
   input += synapse0x25ef3a0();
   input += synapse0x25ef3e0();
   input += synapse0x25ef420();
   input += synapse0x25ef460();
   input += synapse0x25ef4a0();
   input += synapse0x25ef4e0();
   return input;
}

double TMVAClassification_TMlpANN::neuron0x25ef180() {
   double input = input0x25ef180();
   return (input * 1)+0;
}

double TMVAClassification_TMlpANN::synapse0x25eb3f0() {
   return (neuron0x25e9c90()*-3.85957);
}

double TMVAClassification_TMlpANN::synapse0x25eb430() {
   return (neuron0x25e9fd0()*-4.65028);
}

double TMVAClassification_TMlpANN::synapse0x25eb470() {
   return (neuron0x25ea310()*-1.12188);
}

double TMVAClassification_TMlpANN::synapse0x25eb4b0() {
   return (neuron0x25ea650()*-6.88342);
}

double TMVAClassification_TMlpANN::synapse0x25eb4f0() {
   return (neuron0x25ea990()*-1.76494);
}

double TMVAClassification_TMlpANN::synapse0x25eb530() {
   return (neuron0x25eacd0()*5.47621);
}

double TMVAClassification_TMlpANN::synapse0x25eb8b0() {
   return (neuron0x25e9c90()*0.252389);
}

double TMVAClassification_TMlpANN::synapse0x25eb8f0() {
   return (neuron0x25e9fd0()*0.177042);
}

double TMVAClassification_TMlpANN::synapse0x25eb930() {
   return (neuron0x25ea310()*0.0174511);
}

double TMVAClassification_TMlpANN::synapse0x25eb970() {
   return (neuron0x25ea650()*-1.38002);
}

double TMVAClassification_TMlpANN::synapse0x25eb9b0() {
   return (neuron0x25ea990()*-1.07984);
}

double TMVAClassification_TMlpANN::synapse0x25eb9f0() {
   return (neuron0x25eacd0()*4.90449);
}

double TMVAClassification_TMlpANN::synapse0x25ebd70() {
   return (neuron0x25e9c90()*4.56147);
}

double TMVAClassification_TMlpANN::synapse0x25ebdb0() {
   return (neuron0x25e9fd0()*1.26631);
}

double TMVAClassification_TMlpANN::synapse0x25ebdf0() {
   return (neuron0x25ea310()*-4.23442);
}

double TMVAClassification_TMlpANN::synapse0x25ebe30() {
   return (neuron0x25ea650()*3.77307);
}

double TMVAClassification_TMlpANN::synapse0x25ebe70() {
   return (neuron0x25ea990()*2.42264);
}

double TMVAClassification_TMlpANN::synapse0x2429a70() {
   return (neuron0x25eacd0()*3.98619);
}

double TMVAClassification_TMlpANN::synapse0x2429ab0() {
   return (neuron0x25e9c90()*1.91769);
}

double TMVAClassification_TMlpANN::synapse0x25ec300() {
   return (neuron0x25e9fd0()*-1.66059);
}

double TMVAClassification_TMlpANN::synapse0x25ec340() {
   return (neuron0x25ea310()*1.1295);
}

double TMVAClassification_TMlpANN::synapse0x25ec380() {
   return (neuron0x25ea650()*3.38243);
}

double TMVAClassification_TMlpANN::synapse0x25ec3c0() {
   return (neuron0x25ea990()*-2.84022);
}

double TMVAClassification_TMlpANN::synapse0x25ec400() {
   return (neuron0x25eacd0()*3.00662);
}

double TMVAClassification_TMlpANN::synapse0x25ec780() {
   return (neuron0x25e9c90()*0.529513);
}

double TMVAClassification_TMlpANN::synapse0x25ec7c0() {
   return (neuron0x25e9fd0()*-2.78633);
}

double TMVAClassification_TMlpANN::synapse0x25ec800() {
   return (neuron0x25ea310()*3.61229);
}

double TMVAClassification_TMlpANN::synapse0x25ec840() {
   return (neuron0x25ea650()*0.471514);
}

double TMVAClassification_TMlpANN::synapse0x25ec880() {
   return (neuron0x25ea990()*-3.18138);
}

double TMVAClassification_TMlpANN::synapse0x25ec8c0() {
   return (neuron0x25eacd0()*-1.95396);
}

double TMVAClassification_TMlpANN::synapse0x25ecc40() {
   return (neuron0x25e9c90()*1.53803);
}

double TMVAClassification_TMlpANN::synapse0x25ecc80() {
   return (neuron0x25e9fd0()*-2.06494);
}

double TMVAClassification_TMlpANN::synapse0x25eccc0() {
   return (neuron0x25ea310()*1.4977);
}

double TMVAClassification_TMlpANN::synapse0x25ebeb0() {
   return (neuron0x25ea650()*-3.01743);
}

double TMVAClassification_TMlpANN::synapse0x25ebef0() {
   return (neuron0x25ea990()*-3.53876);
}

double TMVAClassification_TMlpANN::synapse0x25ebf30() {
   return (neuron0x25eacd0()*4.60532);
}

double TMVAClassification_TMlpANN::synapse0x25ebf70() {
   return (neuron0x25e9c90()*2.26092);
}

double TMVAClassification_TMlpANN::synapse0x25ed250() {
   return (neuron0x25e9fd0()*-3.73207);
}

double TMVAClassification_TMlpANN::synapse0x25ed290() {
   return (neuron0x25ea310()*6.69914);
}

double TMVAClassification_TMlpANN::synapse0x25ed2d0() {
   return (neuron0x25ea650()*0.360024);
}

double TMVAClassification_TMlpANN::synapse0x25ed310() {
   return (neuron0x25ea990()*-4.94074);
}

double TMVAClassification_TMlpANN::synapse0x25ed350() {
   return (neuron0x25eacd0()*-2.76498);
}

double TMVAClassification_TMlpANN::synapse0x25ed6d0() {
   return (neuron0x25eb140()*0.784683);
}

double TMVAClassification_TMlpANN::synapse0x25ed710() {
   return (neuron0x25eb570()*-0.0743092);
}

double TMVAClassification_TMlpANN::synapse0x25ed750() {
   return (neuron0x25eba30()*0.380374);
}

double TMVAClassification_TMlpANN::synapse0x25ed790() {
   return (neuron0x25ebfc0()*-2.78697);
}

double TMVAClassification_TMlpANN::synapse0x25ed7d0() {
   return (neuron0x25ec440()*2.53916);
}

double TMVAClassification_TMlpANN::synapse0x25ed810() {
   return (neuron0x25ec900()*2.54955);
}

double TMVAClassification_TMlpANN::synapse0x25ed850() {
   return (neuron0x25ecf10()*-1.04588);
}

double TMVAClassification_TMlpANN::synapse0x25edbd0() {
   return (neuron0x25eb140()*0.4384);
}

double TMVAClassification_TMlpANN::synapse0x25edc10() {
   return (neuron0x25eb570()*-1.20955);
}

double TMVAClassification_TMlpANN::synapse0x25edc50() {
   return (neuron0x25eba30()*1.68078);
}

double TMVAClassification_TMlpANN::synapse0x25edc90() {
   return (neuron0x25ebfc0()*-0.665033);
}

double TMVAClassification_TMlpANN::synapse0x25edcd0() {
   return (neuron0x25ec440()*0.647396);
}

double TMVAClassification_TMlpANN::synapse0x25edd10() {
   return (neuron0x25ec900()*-0.293936);
}

double TMVAClassification_TMlpANN::synapse0x25edd50() {
   return (neuron0x25ecf10()*0.177898);
}

double TMVAClassification_TMlpANN::synapse0x25ee0d0() {
   return (neuron0x25eb140()*-1.22032);
}

double TMVAClassification_TMlpANN::synapse0x25ee110() {
   return (neuron0x25eb570()*-0.907469);
}

double TMVAClassification_TMlpANN::synapse0x25ee150() {
   return (neuron0x25eba30()*2.37666);
}

double TMVAClassification_TMlpANN::synapse0x25ee190() {
   return (neuron0x25ebfc0()*1.70748);
}

double TMVAClassification_TMlpANN::synapse0x25ee1d0() {
   return (neuron0x25ec440()*-0.173436);
}

double TMVAClassification_TMlpANN::synapse0x25ee210() {
   return (neuron0x25ec900()*-3.0675);
}

double TMVAClassification_TMlpANN::synapse0x25ee250() {
   return (neuron0x25ecf10()*1.91252);
}

double TMVAClassification_TMlpANN::synapse0x245ef60() {
   return (neuron0x25eb140()*-0.700554);
}

double TMVAClassification_TMlpANN::synapse0x245efa0() {
   return (neuron0x25eb570()*0.907683);
}

double TMVAClassification_TMlpANN::synapse0x242a2f0() {
   return (neuron0x25eba30()*-0.355214);
}

double TMVAClassification_TMlpANN::synapse0x25d8f00() {
   return (neuron0x25ebfc0()*1.65285);
}

double TMVAClassification_TMlpANN::synapse0x25d8f40() {
   return (neuron0x25ec440()*-1.53046);
}

double TMVAClassification_TMlpANN::synapse0x25d8f80() {
   return (neuron0x25ec900()*-0.239076);
}

double TMVAClassification_TMlpANN::synapse0x2458180() {
   return (neuron0x25ecf10()*0.858741);
}

double TMVAClassification_TMlpANN::synapse0x24582e0() {
   return (neuron0x25eb140()*2.79491);
}

double TMVAClassification_TMlpANN::synapse0x25ece90() {
   return (neuron0x25eb570()*-2.38786);
}

double TMVAClassification_TMlpANN::synapse0x25eced0() {
   return (neuron0x25eba30()*2.96434);
}

double TMVAClassification_TMlpANN::synapse0x25eeb80() {
   return (neuron0x25ebfc0()*-3.82496);
}

double TMVAClassification_TMlpANN::synapse0x25eebc0() {
   return (neuron0x25ec440()*-1.33981);
}

double TMVAClassification_TMlpANN::synapse0x25eec00() {
   return (neuron0x25ec900()*2.07409);
}

double TMVAClassification_TMlpANN::synapse0x25eec40() {
   return (neuron0x25ecf10()*3.69432);
}

double TMVAClassification_TMlpANN::synapse0x25eefc0() {
   return (neuron0x25eb140()*-0.136953);
}

double TMVAClassification_TMlpANN::synapse0x25ef000() {
   return (neuron0x25eb570()*0.339259);
}

double TMVAClassification_TMlpANN::synapse0x25ef040() {
   return (neuron0x25eba30()*-1.11453);
}

double TMVAClassification_TMlpANN::synapse0x25ef080() {
   return (neuron0x25ebfc0()*-0.561756);
}

double TMVAClassification_TMlpANN::synapse0x25ef0c0() {
   return (neuron0x25ec440()*0.518835);
}

double TMVAClassification_TMlpANN::synapse0x25ef100() {
   return (neuron0x25ec900()*0.627972);
}

double TMVAClassification_TMlpANN::synapse0x25ef140() {
   return (neuron0x25ecf10()*-0.581039);
}

double TMVAClassification_TMlpANN::synapse0x25ef3a0() {
   return (neuron0x25ed390()*-2.51957);
}

double TMVAClassification_TMlpANN::synapse0x25ef3e0() {
   return (neuron0x25ed890()*-1.69997);
}

double TMVAClassification_TMlpANN::synapse0x25ef420() {
   return (neuron0x25edd90()*1.57619);
}

double TMVAClassification_TMlpANN::synapse0x25ef460() {
   return (neuron0x25ee290()*1.3173);
}

double TMVAClassification_TMlpANN::synapse0x25ef4a0() {
   return (neuron0x25ecd00()*4.10331);
}

double TMVAClassification_TMlpANN::synapse0x25ef4e0() {
   return (neuron0x25eec80()*1.07444);
}

