#!/bin/bash
# Script to run an analysis on Google trend data
./../../build/examples/MakeGoogleTrendsTuple/gtnd --spx_file "../../db/SPX.csv" --begin_date "2000-01-01" --end_date "2009-01-01" --trend_dir "../../db/GoogleTrends"
./../../build/examples/GoogleMVAAnalysis/googlemva --input_file "outputFile_Google_SPX.root" --tree_path "google" --output_file "outputFile_Google_SPX_withMVA.root"
./../../build/examples/GoogleMVABacktester/googlemvabacktest --spx_file "../../db/SPX.csv" --begin_date "2009-01-01" --end_date "2013-06-01" --trend_dir "../../db/GoogleTrends" --mva_type LD --cut_value 0.2
