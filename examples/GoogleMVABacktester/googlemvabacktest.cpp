// My test of the class functionality

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <set>

// Boost
#include <boost/program_options.hpp>


// include local
#include "DefinedMVAs.hpp"
#include "TMVAClassification.hpp"
#include "TMVAReader.hpp"
#include "TreeWriter.hpp"

#include "GoogleMVABacktester.hpp"

// Hudson
#include <YahooDriver.hpp>
#include <DMYCloseDriver.hpp>
#include <EODSeries.hpp>
#include <Database.hpp>
#include <PositionsReport.hpp>
#include <PositionFactors.hpp>
#include <PositionFactorsSet.hpp>
#include <BnHTrader.hpp>
#include <EOMReturnFactors.hpp>
#include <EOMReport.hpp>
#include <PortfolioReturns.hpp>
#include <PortfolioReport.hpp>

using namespace std;
using namespace boost::gregorian;
using namespace Series;

namespace po = boost::program_options;


bool evaluate = true;


int main(int argc, const char* argv[] ) {

	// SPX
	std::string begin_date, end_date;
	std::string spx_dbfile, trendsDirectory;
	Float_t cutValue = 0.0;
	std::string mvaMethod("BDTD");
	int dayshift = 7;
	int period = 3;
	/*
	 * Extract simulation options
	 */
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("spx_file",   po::value<string>(&spx_dbfile),     "SPX series database")
		("begin_date", po::value<string>(&begin_date),     "start of trading period (YYYY-MM-DD)")
		("end_date",   po::value<string>(&end_date),       "end of trading period (YYYY-MM-DD)")
  		("trend_dir",  po::value<std::string>(&trendsDirectory),     	"location of the Google trend files to be read in")
		("mva_type",   po::value<string>(&mvaMethod),      "the mva type you wish to test (BDTD)")
		("day_shift",   po::value<int>(&dayshift),         "time period (day shift) for calculating signal and background weights (7)") 
		("iapp_period",   po::value<int>(&period),         "the indicator app period for calculating the SMA difference (3)") 
		("cut_value",  po::value<Float_t>(&cutValue),      "the minimum mva cut value in which to buy an asset")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if( vm.count("help") ) {
		cout << desc << endl;
		exit(0);
	}

	if( vm["spx_file"].empty() ||
			vm["begin_date"].empty() || vm["end_date"].empty() || vm["trend_dir"].empty()  ) {
		cout << desc << endl;
		exit(1);
	}

	/*
	 * Load series data
	 */
	date load_begin(from_simple_string(begin_date));
	if( load_begin.is_not_a_date() ) {
		cerr << "Invalid begin date " << begin_date << endl;
		exit(EXIT_FAILURE);
	}

	date load_end(from_simple_string(end_date));
	if( load_end.is_not_a_date() ) {
		cerr << "Invalid end date " << end_date << endl;
		exit(EXIT_FAILURE);
	}


	std::string outputFileName(""),  treepath = "data";
	try {

		/*
		 * Load series data
		 */
		date load_begin(from_simple_string(begin_date));
		if( load_begin.is_not_a_date() ) {
			cerr << "Invalid begin date " << begin_date << endl;
			exit(EXIT_FAILURE);
		}

		date load_end(from_simple_string(end_date));
		if( load_end.is_not_a_date() ) {
			cerr << "Invalid end date " << end_date << endl;
			exit(EXIT_FAILURE);
		}
		const string spx_symbol = "index:SPX";
		std::cout << "Loading " << spx_dbfile << " from " << load_begin << " to " << load_end << "..." << std::endl;
		Series::EODDB::instance().load(spx_symbol, spx_dbfile, Series::EODDB::YAHOO, load_begin, load_end);
		const Series::EODSeries& spx_db = Series::EODDB::instance().get(spx_symbol);

		NN::TreeWriter treeWriter( "mva_backtester.root", "google" );
		treeWriter.add( "mvaCutValue");
		treeWriter.add( "ROI");
		treeWriter.add( "STDDEV");
		treeWriter.add( "SKEW");
		treeWriter.add( "AVE");
		treeWriter.add( "nPositions");

		if( evaluate ) {



			std::vector<std::string> inputvars;
			inputvars.push_back("GTND_crisis"); 
			inputvars.push_back("GTND_earnings" ); 
			inputvars.push_back("GTND_economy"); 
			inputvars.push_back("GTND_gold"); 
			inputvars.push_back("GTND_markets"); 
			inputvars.push_back("GTND_stocks"); 

			/*inputvars.push_back("GTND_bubble"); 
			  inputvars.push_back("GTND_depression"); 
			  inputvars.push_back("GTND_economy"); 
			  inputvars.push_back("GTND_markets");*/ 

			std::cout << "SPX Records: " << spx_db.size() << std::endl;
			std::cout << "SPX Period: " << spx_db.period() << std::endl;
			std::cout << "SPX Total days: " << spx_db.duration().days() << std::endl;

			// Initialize and run strategy
			int N(1); 
			//Float_t cutval_min(cutValue), cutval_max(1.0);
			for (int i(0); i < N ; ++i ) {
				GoogleMVABacktester backtester( spx_db, trendsDirectory, mvaMethod, cutValue);
				//backtester.setCutValue( cutvalue );
				backtester.setup( inputvars, period, dayshift, "weights"); 
				//cutValue = cutval_min + ( ( cutval_max - cutval_min) / N ) * i;
				backtester.setCutValue( cutValue );
				backtester.run();

				std::cout <<  "Tester with mva value @ " << cutValue << std::endl;
				// Print trades  
				Report::header("Closed trades");
				backtester.positions("SPX").closed().print();

				//Report::header("Open trades");
				//backtester.positions("SPX").open().print();

				// SPX stats
				Report::header("Strategy trades");
				backtester.positions().stratPos().print();
				// SPX stats
				Report::header("SPX Stats");
				EOMReturnFactors spx_eomrf( backtester.positions("SPX"), load_begin, load_end);
				EOMReport eomrp(spx_eomrf);
				eomrp.print();

				// Print simulation reports
				//Report::header("Trade results");
				//ReturnFactors spx_eomrf( backtester.positions() );
				//Report rp(spx_eomrf);
				//rp.print();

				// Print Position stats
				/*Report::header("Position stats");
				  PositionFactorsSet pf( backtester.positions().stratPos().closed());
				  PositionsReport pr(pf);
				  pr.print();*/

				// Position analysis
				//Report::header("Positions");
				PositionFactorsSet pfs(backtester.positions());
				PositionsReport pr(pfs);
				pr.print();

				// Portfolio stats
				Report::header("Portfolio Stats");
				PortfolioReturns prns;
				prns.add( &spx_eomrf );
				PortfolioReport preport(prns);
				preport.print();
				Report::precision(2);

				/*eomrp.roi();
				  eomrp.cagr();
				  eomrp.gsdm();
				  eomrp.sharpe();
				  eomrp.roi();*/

				treeWriter.column( "mvaCutValue", cutValue );
				treeWriter.column( "ROI", spx_eomrf.roi() );
				treeWriter.column( "STDDEV", spx_eomrf.stddev() );
				treeWriter.column( "SKEW", spx_eomrf.skew() );
				treeWriter.column( "AVE", spx_eomrf.avg() );
				treeWriter.column( "nPositions", spx_eomrf.num() );
				treeWriter.write();
				// BnH
				/*Report::header("SPX BnH");
				  BnHTrader bnh(spx_db); 
				  bnh.run();
				  bnh.positions().print();
				  EOMReturnFactors bnh_eomrf(bnh.positions(), load_begin, load_end);
				  EOMReport bnh_rp(bnh_eomrf);
				 */

			}
		}

	} catch( std::exception& ex ) {

		std::cerr << "Unhandled exception: " << ex.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	return 0;
}
