/*
 * Copyright (C) 2007,2008 Alberto Giannetti
 *
 * This file is part of Hudson.
 *
 * Hudson is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hudson is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hudson.  If not, see <http://www.gnu.org/licenses/>.
 */
 
// STL
#include <cmath>

// Hudson
#include "GoogleMVABacktester.hpp"
#include "string_tools.hpp"

// Root
#include "TSystemDirectory.h"


using namespace std;
using namespace boost::gregorian;
using namespace Series;


//********************************************************************************
GoogleMVABacktester::GoogleMVABacktester( const EODSeries& db, const std::string& directory, const std::string& mva,  const Float_t& cutValue)
 :
  m_db( db ),
  m_mvaName( mva ),
  m_directory( directory+"/" ),
  m_cutValue( cutValue ),
  m_setup( false ),
  m_reader( 0 )
{
}


//********************************************************************************
void GoogleMVABacktester::run(void) throw(TraderException)
{
  std::cout << "Running simulation.\n";
 if (! m_setup ) {
    std::cerr << "GoogleMVABacktester: run - Cannot run as setup has not been set.\n";
    exit(EXIT_FAILURE);
    return;
  }

  std::cout << "Running simulation.\n";
  Series::EODSeries::const_iterator iter( m_db.begin() ); 
  std::cout << "Running simulation.\n";

  std::advance( iter, m_indicatorApp[ m_inputvars[0] ]->getStartIdx() ); // All indicatorApps should by construction have the same offset
  std::cout << "Running simulation.\n";
  TMVA::Timer timer( std::distance(iter, m_db.end() ), "GoogleMVABacktester", kTRUE ); 
  std::cout << "Running simulation.\n";

  for( int i = 0; iter != m_db.end(); ++iter, ++i ) {
    try {

      trade( iter );
      timer.DrawProgressBar( i );
    } catch( std::exception& e ) {

      cerr << e.what() << endl;
      continue;
    }
  }
}


//********************************************************************************
void GoogleMVABacktester::trade( Series::EODSeries::const_iterator& iter )
{
  Float_t mvaValue(0.0), value(0.0), error(0.);

  // now loop over leaves and set the values
  std::vector< std::string >::const_iterator it = m_inputvars.begin();
  const std::vector< std::string >::const_iterator endit = m_inputvars.end();
  //bool ok(true);
  std::cout << m_inputvars.size() << std::endl;
  boost::gregorian::date d1;
  for ( ; it != endit; ++it ) {    
    std::string name = *it;
    std::cout << "NAME is " << name << " @ date " << iter->first << std::endl; 
    value = static_cast<Float_t>(  m_indicatorApp[name]->evaluateBefore( "GTND", iter->first ) );
    m_reader->setVariable( name, value );
  }

  // get the mva output value
  mvaValue = m_reader->getMvaValue( m_mvaName );
  error = m_reader->getMvaError( m_mvaName );
  std::cout << "MVA value " << mvaValue << " and the error is " << error << std::endl; 
  check_buy( iter, mvaValue );

  //check_sell(db, iter, macd);
}

//********************************************************************************
void GoogleMVABacktester::setup( const std::vector< std::string >& inputvars, const int& period, const int& dayshift, const std::string& weightsDirPrefix ) {
 
  // delete anything that has been set before now.
  if( m_reader ) {
    delete m_reader; m_reader=0;
  }  

  m_inputvars = inputvars;
  std::vector< std::string > mvaMethods(1);
  mvaMethods[0] = m_mvaName;
  // setup the TMVAReader class
  m_reader = new NN::TMVAReader( mvaMethods, inputvars, "dummy.root", weightsDirPrefix );

  setPeriodAndSignalShift( period, dayshift );
  
  m_setup=true; 
}

//********************************************************************************
void GoogleMVABacktester::check_buy( Series::EODSeries::const_iterator& iter, const Float_t& value )
{
  // Buy on MACD cross and MACD Hist > 0.5%
  if( _miPositions.open( m_db.name() ).empty() &&  
       m_db.after(iter->first, 7) != m_db.end() ) {
    if( value < m_cutValue ) return; 
    cout << m_db.name() << " value " << value << endl;
    
    // Buy tomorrow's open
    EODSeries::const_iterator iter_entry = m_db.after(iter->first);
    if( iter_entry == m_db.end() ) {
      cerr << "Warning: can't open " << m_db.name() << " position after " << iter->first << endl;
      return;
    }
    cout << "Buying on " << iter_entry->first << " at " << iter_entry->second.open << endl;
    buy( m_db.name(), iter_entry->first, Price( iter_entry->second.open ) );

    // Sell 7 days from now!
    EODSeries::const_iterator iter_exit = m_db.after(iter->first, 8);
    if( iter_exit == m_db.end() ) {
      cerr << "Warning: can't close " << m_db.name() << " position after " << iter->first << endl;
      return;
    }
    // Close all open positions at tomorrow's open
    PositionSet ps = _miPositions.open( m_db.name() );
    for( PositionSet::const_iterator pos_iter = ps.begin(); pos_iter != ps.end(); ++pos_iter ) {
      PositionPtr pPos = (*pos_iter);
      // Sell at tomorrow's open
      //cout << "Selling on " << iter_exit->first << " at " << iter_exit->second.open << endl;
      close(pPos->id(), iter_exit->first, Price( iter_exit->second.open ) );
    } // end of all open positions
  }
}



//********************************************************************************
GoogleMVABacktester::~GoogleMVABacktester( ) 
{ 
  if( !m_indicatorApp.empty() ) {
    std::map< std::string, IndicatorApp* >::iterator iter = m_indicatorApp.begin();
    const std::map< std::string, IndicatorApp* >::iterator end = m_indicatorApp.end();
    for (; iter != end; ++iter ) {
      if( iter->second ) {
        delete iter->second; iter->second=0;
      }
    }	
      
  }
//delete m_treeWriter; m_treeWriter=0;
}


//********************************************************************************
bool GoogleMVABacktester::getListOfFiles( const char *dirname, const char *ext, const int& period) {

  bool status = false;
  TSystemDirectory dir( dirname, dirname );
  TList *files = dir.GetListOfFiles();
  if( files->IsEmpty() )
    return status;
  files->Sort(kTRUE);
  boost::gregorian::date begin = m_db.begin()->first;
  boost::gregorian::date end = m_db.last().key;

  std::string name("");
  string_tools strtools;
  if ( files ) {
    TSystemFile *file = 0;;
    TString fname;
    TIter next(files);
    while ( ( file = ( TSystemFile* ) next() ) ) {
      fname = file->GetName();
      name = fname.Data();
      if ( !file->IsDirectory() && fname.EndsWith( ext ) ) {
        strtools.replaceAfter( name, "_" );
        Series::EODDB::instance().load( name, TString(dirname + fname).Data(), Series::EODDB::GOOGLETREND, begin, end );
        //m_inputvars.push_back( name );//m_inputFileNames.insert( std::make_pair( name, dbseries ) );
        std::cout << "ADDING TO IndicatorAPP for " << name << std::endl;
        //m_indicatorApp.insert( std::make_pair( name, Series::EODDB::instance().get( name ) ) );
        //m_inputFileNames[name] = &Series::EODDB::instance().get( name );
	std::string newname = "GTND_"+name;
	m_indicatorApp[newname] = new IndicatorApp( Series::EODDB::instance().get( name )  );
        m_indicatorApp[newname]->add( "GTND", period );
        m_indicatorApp[newname]->initialise();
      } 
    }
  }
  status = true;
  return status;
}

//********************************************************************************
bool GoogleMVABacktester::setPeriodAndSignalShift( const int& period, const int& dayshift ) {
 
  std::cout << "INFO: GoogleTrendReader - adding Google trend files from directory " << m_directory << std::endl; 
  std::cout << "INFO: GoogleTrendReader - period is "<< period << " and days shifted to calculate weighting is " << dayshift << std::endl; 
  bool status =  getListOfFiles( m_directory.c_str(), ".csv", period ) ;
  if(!status) return status;
  m_dayshift = dayshift;
  // Advance the iterator to start of indicator data
  // stops hitting the last entry when calculating the signal like events, since we look to
  // the future.
  
  return status;
}


/*void GoogleMVABacktester::check_sell( const EOMSeries& db, EOMSeries::const_iterator& iter, const TA::MACDRes& macd, int i )
{
  if( ! _miPositions.open(db.name()).empty() && macd.macd[i] < macd.macd_signal[i] && (::abs(macd.macd_hist[i]) / iter->second.close ) > 0.005 ) {
  
    //cout << db.name() << "MACD " << macd.macd[i] << " MACD Signal " << macd.macd_signal[i] << " MACD Hist " << macd.macd_hist[i] << endl;
    
    EOMSeries::const_iterator iter_exit = db.after(iter->first);
    if( iter_exit == db.end() ) {
      cerr << "Warning: can't close " << db.name() << " position after " << iter->first << endl;
      return;
    }

    // Close all open positions at tomorrow's open
    PositionSet ps = _miPositions.open(db.name());
    for( PositionSet::const_iterator pos_iter = ps.begin(); pos_iter != ps.end(); ++pos_iter ) {
      PositionPtr pPos = (*pos_iter);
      // Sell at tomorrow's open
      //cout << "Selling on " << iter_exit->first << " at " << iter_exit->second.open << endl;
      close(pPos->id(), iter_exit->first, Price(iter_exit->second.open));
    } // end of all open positions
  }
}*/
