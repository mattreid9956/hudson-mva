
add_library(EOMTrader SHARED EOMTrader.cpp  EOMTrader.hpp)
add_executable(eom eom.cpp)
add_executable(eom_matrix eom_matrix.cpp)

target_link_libraries(eom EOMTrader Hudson )
target_link_libraries(eom_matrix EOMTrader Hudson )
 
#FILE(GLOB_RECURSE COMPONENT_TXT_FILE_LIST "*.txt")
#FILE(COPY ${COMPONENT_TXT_FILE_LIST} DESTINATION .)

install(TARGETS eom
  # IMPORTANT: Add the eom executable to the "export-set"
  EXPORT HudsonTargets
  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin)


install(TARGETS eom_matrix
  # IMPORTANT: Add the eom_matrix executable to the "export-set"
  EXPORT HudsonTargets
  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin)
