/*
 * Copyright (C) 2007,2008 Alberto Giannetti
 *
 * This file is part of Hudson.
 *
 * Hudson is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hudson is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hudson.  If not, see <http://www.gnu.org/licenses/>.
 */

// STD
#include <iostream>
#include <string>

// Boost
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/program_options.hpp>

// Hudson
#include <Database.hpp>
#include <EOMReturnFactors.hpp>
#include <PositionFactorsSet.hpp>
#include <BnHTrader.hpp>
#include <EOMReport.hpp>
#include <PositionsReport.hpp>

// App
#include "EOMTrader.hpp"

using namespace std;
using namespace boost::gregorian;
using namespace Series;

namespace po = boost::program_options;



int main(int argc, char* argv[])
{
  std::string begin_date, end_date;
  std::string spx_dbfile;
  /*
   * Extract simulation options
   */
  po::options_description desc("Allowed options");
  desc.add_options()
 	("help", "produce help message")
	("spx_file",   po::value<string>(&spx_dbfile),     "SPX series database")
	("begin_date", po::value<string>(&begin_date),     "start of trading period (YYYY-MM-DD)")
	("end_date",   po::value<string>(&end_date),       "end of trading period (YYYY-MM-DD)")
	;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if( vm.count("help") ) {
  	cout << desc << endl;
  	exit(0);
  }

  if( vm["spx_file"].empty() ||
  		vm["begin_date"].empty() || vm["end_date"].empty() ) {
  	cout << desc << endl;
  	exit(1);
  }

  /*
   * Load series data
   */
  date load_begin(from_simple_string(begin_date));
  if( load_begin.is_not_a_date() ) {
  	cerr << "Invalid begin date " << begin_date << endl;
  	exit(EXIT_FAILURE);
  }
  date load_end(from_simple_string(end_date));
  if( load_end.is_not_a_date() ) {
  	cerr << "Invalid end date " << end_date << endl;
  	exit(EXIT_FAILURE);
  }

  try {

    /*
     * Load series data
     */
    date load_begin(from_simple_string(begin_date));
    if( load_begin.is_not_a_date() ) {
        cerr << "Invalid begin date " << begin_date << endl;
	exit(EXIT_FAILURE);
    }

    date load_end(from_simple_string(end_date));
    if( load_end.is_not_a_date() ) {
	cerr << "Invalid end date " << end_date << endl;
    	exit(EXIT_FAILURE);
    }
    const string spx_symbol = "SPX";
    std::cout << "Loading " << spx_dbfile << " from " << load_begin << " to " << load_end << "..." << std::endl;
    Series::EODDB::instance().load(spx_symbol, spx_dbfile, Series::EODDB::YAHOO, load_begin, load_end);
    const Series::EODSeries& spx_db = Series::EODDB::instance().get(spx_symbol);

    EOMTrader trader("SPX", spx_db);
    trader.run(3, 3);

    /*
     * Print open/closed positions
     */
    Report::header("Closed trades");
    trader.positions().closed().print();

    Report::header("Open trades");
    trader.positions().open().print();

    /*
     * Print simulation reports
     */
    Report::header("Trade results");
    ReturnFactors rf(trader.positions());
    Report rp(rf);
    rp.print();

    /*
     * Positions excursion
     */
    Report::header("Positions excursion");
    PositionFactorsSet pf(trader.positions().closed());
    PositionsReport pr(pf);
    pr.print();

    // BnH
    Report::header("BnH");
    BnHTrader bnh(spx_db);
    bnh.run();
    EOMReturnFactors bnh_rf(bnh.positions(), load_begin, load_end);
    EOMReport bnh_rp(bnh_rf);
    bnh_rp.print();

  } catch( std::exception& ex ) {

    cerr << "Unhandled exception: " << ex.what() << endl;
    exit(EXIT_FAILURE);
  }

  return 0;
}
