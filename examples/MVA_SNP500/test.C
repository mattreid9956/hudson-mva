// NEED TO GET all histograms from variable distributions and cmpare the prob of each with the referecne.
#include "../db/macros/tmvaglob.C"

void getHistos( TDirectory* tempdir, TObjArray& sighists, TObjArray& bkghists, TString extname = "Original", Bool_t isRegression = kFALSE );

void test( TString fileName2 = "TMVA1.root" ) {

    // set style and remove existing canvas'
    TMVAGlob::Initialize( true );
    TString referenceF = "MVA_SNP500/TMVA.root";
    // checks if file with name "fin" is already open, and if not opens one
    TFile* file = TFile::Open( referenceF );  
    TDirectory* tempdir = (TDirectory*)file->Get("Method_BDT/BDTD" );
    tempdir->cd();

    //std::vector<TString> classnames(TMVAGlob::GetClassNames(tempdir));
    TObjArray sighists, bkghists;
    getHistos( tempdir, sighists, bkghists );

   gStyle->SetCanvasColor( 0 );
    //cout << "Set histogram style..." << endl;
    TMVAGlob::SetMultiClassStyle( &sighists );
    TMVAGlob::SetMultiClassStyle( &bkghists );


    TFile* fileCheck = TFile::Open( fileName2 );  
    TDirectory* tempdir2 = (TDirectory*)fileCheck->Get("Method_BDT/BDTD" );
    tempdir2->cd();
    TObjArray otsighists, otbkghists;
    getHistos( tempdir2, otsighists, otbkghists, "newvars" );

    TMVAGlob::SetMultiClassStyle( &otsighists );
    TMVAGlob::SetMultiClassStyle( &otbkghists );

    TCanvas* c1 = new TCanvas("c1","c1", 900, 900./1.618);
    // perform K-S test
    cout << "--- Perform Kolmogorov-Smirnov tests" << endl;
    //    cout << "--- Goodness of consistency for class " << classnames.at(icls)<< endl;
    TString probatext("Kolmogorov-Smirnov test: ");
    double min(0.), max(0.), maxsig(0.), maxbkg(0.);
    int bins(0);
    for(Int_t j(0); j<otsighists.GetEntriesFast(); ++j){
        bins = ((TH1*)sighists[j])->GetNbinsX();
        min = ( ((TH1*)sighists[j])->GetXaxis()->GetXmin() < ((TH1*)otsighists[j])->GetXaxis()->GetXmin() ) ? ((TH1*)sighists[j])->GetXaxis()->GetXmin() :
            ((TH1*)otsighists[j])->GetXaxis()->GetXmin() ;
        max = ( ((TH1*)sighists[j])->GetXaxis()->GetXmax() > ((TH1*)otsighists[j])->GetXaxis()->GetXmax() ) ? ((TH1*)sighists[j])->GetXaxis()->GetXmax() :
            ((TH1*)otsighists[j])->GetXaxis()->GetXmax() ;

        ((TH1*)sighists[j])->SetBins( bins, min, max);
        ((TH1*)otsighists[j])->SetBins( bins, min, max);
        Float_t kol = ((TH1*)sighists[j])->KolmogorovTest(((TH1*)otsighists[j]));
        cout <<  ((TH1*)sighists[j])->GetName() << ": " << kol  << endl;
        ((TH1*)otsighists[j])->SetLineColor(2);
        ((TH1*)otsighists[j])->SetLineWidth(2);
        if( ((TH1*)sighists[j])->GetMaximum() < ((TH1*)otsighists[j])->GetMaximum() ) {
            ((TH1*)otsighists[j])->Draw("hist");
            ((TH1*)sighists[j])->Draw("histsame");
        }
        else {
            ((TH1*)sighists[j])->Draw("hist");
            ((TH1*)otsighists[j])->Draw("histsame");
        }
        c1->Draw();
        c1->SaveAs( TString(((TH1*)sighists[j])->GetName())+".eps");
        //probatext.Append(classnames.at(j)+Form(" %.3f ",kol));

    }

    for(Int_t j=0; j<otbkghists.GetEntriesFast(); ++j ) {
        bins =((TH1*)bkghists[j])->GetNbinsX();
        min = ( ((TH1*)bkghists[j])->GetXaxis()->GetXmin() < ((TH1*)otbkghists[j])->GetXaxis()->GetXmin() ) ? ((TH1*)bkghists[j])->GetXaxis()->GetXmin() :
            ((TH1*)otbkghists[j])->GetXaxis()->GetXmin() ;
        max = ( ((TH1*)bkghists[j])->GetXaxis()->GetXmax() > ((TH1*)otbkghists[j])->GetXaxis()->GetXmax() ) ? ((TH1*)bkghists[j])->GetXaxis()->GetXmax() :
            ((TH1*)otbkghists[j])->GetXaxis()->GetXmax() ;

        ((TH1*)bkghists[j])->SetBins( bins, min, max);
        ((TH1*)otbkghists[j])->SetBins( bins, min, max);
        Float_t kol = ((TH1*)bkghists[j])->KolmogorovTest(((TH1*)otbkghists[j]));
        cout <<  ((TH1*)bkghists[j])->GetName() << ": " << kol  << endl;
        ((TH1*)otbkghists[j])->SetLineColor(2);
        ((TH1*)otbkghists[j])->SetLineWidth(2);
        if( ((TH1*)bkghists[j])->GetMaximum() < ((TH1*)otbkghists[j])->GetMaximum() ) {
            ((TH1*)otbkghists[j])->Draw("hist");
            ((TH1*)bkghists[j])->Draw("histsame");
        }
        else {
            ((TH1*)bkghists[j])->Draw("hist");
            ((TH1*)otbkghists[j])->Draw("histsame");
        }
        c1->Draw();
        c1->SaveAs( TString(((TH1*)bkghists[j])->GetName())+".eps");
        //probatext.Append(classnames.at(j)+Form(" %.3f ",kol));
    }

    //TText* tt = new TText( 0.12, 0.74, probatext );
    //tt->SetNDC(); tt->SetTextSize( 0.032 ); tt->AppendPad();



    // redraw axes
    //frame->Draw("sameaxis");

}

void getHistos( TDirectory* tempdir, TObjArray& sighists, TObjArray& bkghists, TString extname, Bool_t isRegression ){

    TIter next(tempdir->GetListOfKeys());
    while ((key = (TKey*)next())) {
        if (key->GetCycle() != 1) continue;

        if (!TString(key->GetName()).Contains("__Signal") && 
                !(isRegression && TString(key->GetName()).Contains("__Regression"))) continue;

        // make sure, that we only look at histograms
        TClass *cl = gROOT->GetClass(key->GetClassName());
        if (!cl->InheritsFrom("TH1")) continue;
        TH1 *sig = (TH1*)key->ReadObj();
        TString hname(sig->GetName());

        //TMVAGlob::SetFrameStyle( sig, 1.2 );
        // find the corredponding backgrouns histo
        TString bgname = hname;
        bgname.ReplaceAll("__Signal","__Background");
        TH1 *bgd = (TH1*)tempdir->Get(bgname);
        if (bgd == NULL) {
            cout << "ERROR!!! couldn't find background histo for" << hname << endl;
            exit;
        }

        // this is set but not stored during plot creation in MVA_Factory
        TMVAGlob::SetSignalAndBackgroundStyle( sig, (isRegression ? 0 : bgd) );            

        sig->SetTitle( TString( hname ) + ": " + sig->GetTitle() );
        sig->GetYaxis()->SetTitleOffset( 1.70 );

        // normalise both signal and background
        TMVAGlob::NormalizeHists( sig, bgd );
      Float_t sc = 1.1;
      sig->SetMaximum( TMath::Max( sig->GetMaximum(), bgd->GetMaximum() )*sc );

        sighists.Add(sig);
        bkghists.Add(bgd);
    }

}
