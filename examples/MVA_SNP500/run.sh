#!/bin/bash

#export variables="BOP APO STDDEV CCI ADO ADX MACD_signal" # winning combination ;)
export variables="BOP APO STDDEV CCI ADO ADX MACD_signal"
export addmvas="LD TMlpANN BDTD MLP MLPBFGS MLPBNN"
#export addmvas="LD TMlpANN BDTD MLP MLPBFGS MLPBNN"

./../../build/examples/MakeIndicatorTuple/maketuple --spx_file "../../db/SPX.csv" --begin_date "2001-01-01" --end_date "2010-01-01"

./../../build/examples/MVAAnalysis/mva --input_file "outputFile_SPX.root" --tree_path "data" --output_file "outputFile_SPX_withMVA.root" -e -t --mva $addmvas --var $variables

# view the output of the networks
root -l ../../db/macros/TMVAGui.C 

# Do some testing on a unique dataset to see if the tmva performs like it thinks it will. We use a 2 year peroid from 2010-2012.
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2010-01-02" --end_date "2012-12-01" --mva_type BDTD --cut_min -0.3 --cut_max 0.271 --iters 150 --cut_type 0 --output_file "mva_backtester_standardBDTD.root" --var $variables
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2010-01-02" --end_date "2012-12-01" --mva_type TMlpANN --cut_min -0.2 --cut_max 1.1 --iters 150 --cut_type 0 --output_file "mva_backtester_standardTMlpANN.root" --var $variables
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2010-01-02" --end_date "2012-12-01" --mva_type MLP --cut_min -0.3 --cut_max 0.4 --iters 150 --cut_type 0 --output_file "mva_backtester_standardMLP.root" --var $variables
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2010-01-02" --end_date "2012-12-01" --mva_type MLPBNN --cut_min -0.3 --cut_max 1.2 --iters 150 --cut_type 0 --output_file "mva_backtester_standardMLPBNN.root" --var $variables
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2010-01-02" --end_date "2012-12-01" --mva_type MLPBFGS --cut_min -0.3 --cut_max 1.2 --iters 150 --cut_type 0 --output_file "mva_backtester_standardMLPBFGS.root" --var $variables


# Run the ROOT macro and determine optimal cut in which to use on "real" data.
root -l optimise_macro.C\(\"mva_backtester_standardMLPBFGS.root\"\,\"MLPBFGS\"\)
root -l optimise_macro.C\(\"mva_backtester_standardMLPBNN.root\"\,\"MLPBNN\"\)
root -l optimise_macro.C\(\"mva_backtester_standardMLP.root\"\,\"MLP\"\)
root -l optimise_macro.C\(\"mva_backtester_standardTMlpANN.root\"\,\"TMlpANN\"\)
root -l optimise_macro.C\(\"mva_backtester_standardBDTD.root\"\,\"BDTD\"\)

# Using the optimal values specified by the optimise macro and TMVAGui efficiencies, run on "real" live data over 2013.
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2013-01-01" --end_date "2013-12-01" --mva_type BDTD --cut_value 0.017 --cut_type 0 --output_file "REAL_mva_backtesterBDTD.root" | tee std_random_BDTD_0017.txt
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2013-01-01" --end_date "2013-12-01" --mva_type TMlpANN --cut_value 0.18 --cut_type 0 --output_file "REAL_mva_backtesterTMlpANN.root" --var $variables | tee std_random_TMlpANN_018.txt
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2013-01-01" --end_date "2013-12-01" --mva_type MLP --cut_value 0.39 --cut_type 0 --output_file "REAL_mva_backtesterMLP.root" --var $variables  | tee std_random_MLP_039.txt

# Apply a random approach and see which is better
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2013-01-01" --end_date "2013-12-01" --mva_type BDTD --cut_value 0.39 --cut_type 1 --iters 200 --output_file "REAL_mva_backtester_randomMLP.root" 
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2013-01-01" --end_date "2013-12-01" --mva_type TMlpANN --cut_value 0.39 --cut_type 1 --iters 200 --output_file "REAL_mva_backtester_randomTMlpANN.root"
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2013-01-01" --end_date "2013-12-01" --mva_type MLP --cut_value 0.39 --cut_type 1 --iters 200 --output_file "REAL_mva_backtester_randomMLP.root"

# Applying no cut at all
./../../build/examples/MVABacktester/mvabacktest --spx_file "../../db/SPX.csv" --begin_date "2013-01-01" --end_date "2013-12-01" --mva_type BDTD --cut_value -999999 --cut_type 0 --output_file "REAL_mva_backtester_NoCut.root" | tee std_NOCUT.txt


